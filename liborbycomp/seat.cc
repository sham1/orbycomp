/*
 * seat.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/seat.hh>
#include <wayland-server-protocol.h>

namespace orbycomp
{

namespace
{

void
remove_from_list(struct wl_resource *resource)
{
	wl_list_remove(wl_resource_get_link(resource));
}

} // namespace

Seat::Seat(const std::string &name, Compositor *compositor)
    : name_{name}, compositor_{compositor},
      global_{wl_global_create(compositor->display(), &wl_seat_interface, 5, this, Seat::on_bind_cb),
	      wl_global_destroy}
{
	wl_list_init(&global_resources_);
}

void
Seat::on_bind_cb(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	Seat *seat = static_cast<Seat *>(data);

	struct wl_resource *seat_resource = wl_resource_create(client, &wl_seat_interface, version, id);
	if (seat_resource == nullptr) {
		wl_client_post_no_memory(client);
		return;
	}

	wl_resource_set_implementation(seat_resource, &Seat::seat_iface, seat, remove_from_list);

	wl_list_insert(seat->global_resources_.prev, wl_resource_get_link(seat_resource));

	if (version >= 2) {
		wl_seat_send_name(seat_resource, seat->name_.c_str());
	}

	wl_seat_send_capabilities(seat_resource, seat->caps_.to_cap_bits());
}

void
Seat::get_pointer(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
	Seat *self = static_cast<Seat *>(wl_resource_get_user_data(resource));

	if (self->pointer_) {
		try {
			self->pointer_.value().create_resource(client, id, wl_resource_get_version(resource));
		} catch (const std::bad_alloc &) {
			wl_resource_post_no_memory(resource);
		}
	}
}

void
Seat::get_keyboard(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
	Seat *self = static_cast<Seat *>(wl_resource_get_user_data(resource));

	if (self->keyboard_) {
		try {
			self->keyboard_.value().create_resource(client, id,
								wl_resource_get_version(resource));
		} catch (const std::bad_alloc &) {
			wl_resource_post_no_memory(resource);
		} catch (const std::exception &e) {
			wl_resource_post_error(resource, WL_DISPLAY_ERROR_NO_MEMORY, "%s", e.what());
		}
	}
}

void
Seat::get_touch(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
}

void
Seat::release(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

Seat::~Seat()
{
	compositor_->on_seat_destroy_signal(this);

	struct wl_resource *resource, *tmp;
	wl_resource_for_each_safe(resource, tmp, &global_resources_) { wl_resource_destroy(resource); }
}

void
Seat::update_caps(const Capabilities &old_caps)
{
	if (caps_ != old_caps) {
		struct wl_resource *resource;
		wl_resource_for_each(resource, &global_resources_)
		{
			wl_seat_send_capabilities(resource, caps_.to_cap_bits());
		}

		auto [has_keyboard, has_pointer, has_touch] =
		    std::make_tuple(caps_.keyboards > 0, caps_.pointers > 0, caps_.touch_devices > 0);

		if (has_keyboard) {
			if (!keyboard_) {
				keyboard_.emplace(this);
			}
		}

		if (has_pointer) {
			if (!pointer_) {
				pointer_.emplace(this);
			}
		}

		on_caps_changed_signal_(keyboard_, pointer_);
	}
}

void
Seat::add_keyboard_capability()
{
	Capabilities old_caps = caps_;
	++caps_.keyboards;

	update_caps(old_caps);
}

void
Seat::add_pointer_capability()
{
	Capabilities old_caps = caps_;
	++caps_.pointers;

	update_caps(old_caps);
}

void
Seat::add_touch_capability()
{
	Capabilities old_caps = caps_;
	++caps_.touch_devices;

	update_caps(old_caps);
}

void
Seat::remove_keyboard_capability()
{
	Capabilities old_caps = caps_;
	--caps_.keyboards;

	update_caps(old_caps);
}

void
Seat::remove_pointer_capability()
{
	Capabilities old_caps = caps_;
	--caps_.pointers;

	update_caps(old_caps);
}

void
Seat::remove_touch_capability()
{
	Capabilities old_caps = caps_;
	--caps_.touch_devices;

	update_caps(old_caps);
}

bool
Seat::Capabilities::operator==(const Capabilities &other) const
{
	return this->to_cap_bits() == other.to_cap_bits();
}

enum wl_seat_capability
Seat::Capabilities::to_cap_bits() const
{
	auto [has_keyboard, has_pointer, has_touch] =
	    std::make_tuple(keyboards > 0, pointers > 0, touch_devices > 0);

	unsigned int cap_bits{0};

	if (has_keyboard) {
		cap_bits |= WL_SEAT_CAPABILITY_KEYBOARD;
	}

	if (has_pointer) {
		cap_bits |= WL_SEAT_CAPABILITY_POINTER;
	}

	if (has_touch) {
		cap_bits |= WL_SEAT_CAPABILITY_TOUCH;
	}

	return static_cast<enum wl_seat_capability>(cap_bits);
}

} // namespace orbycomp
