/*
 * compositor.cc
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <algorithm>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/surface.hh>
#include <orbycomp/ev-dbus.hh>
#include <unistd.h>

namespace orbycomp
{

void wayland_io_cb(EV_P_ ev_io *w, int revents);
void wayland_prepare_cb(EV_P_ ev_prepare *w, int revents);
static void on_sigint_cb(EV_P_ ev_signal *w, int revents);

Compositor::Compositor(std::unique_ptr<struct wl_display, decltype(&wl_display_destroy)> &&display,
		       std::unique_ptr<struct ev_loop, decltype(&ev_loop_destroy)> &&loop)
    : display_{std::move(display)}, loop_{std::move(loop)}, lua_{this}, keybind_ctx_
{
	this
}
#ifdef ENABLED_DBUS
, system_context{dbus::Context::get_from_bus(loop_.get(), DBUS_BUS_SYSTEM)}, session_context
{
	dbus::Context::get_from_bus(loop_.get(), DBUS_BUS_SESSION)
}
#endif
, cursor_ctx_{xcursor::CursorContext::load_theme()},
    global_{wl_global_create(display_.get(), &wl_compositor_interface, 4, this, Compositor::on_global_bind),
	    wl_global_destroy},
    backend_{load_default_backend(this)}, shell_{this}, xdg_shell_{&shell_}, data_manager_{this},
    xwayland_ctx_{this},
    subcompositor_global_{
	wl_global_create(
	    display_.get(), &wl_subcompositor_interface, 1, this, Compositor::on_subcompositor_bind),
	wl_global_destroy}
{
	create_compositor_global();

	ensure_display();

	int wayland_fd = wl_event_loop_get_fd(wl_display_get_event_loop(display_.get()));

	ev_io_init(&this->wayland_watcher, wayland_io_cb, wayland_fd, EV_READ | EV_WRITE);
	this->wayland_watcher.data = this;
	ev_io_start(loop_.get(), &this->wayland_watcher);

	ev_prepare_init(&this->wayland_prepare, wayland_prepare_cb);
	this->wayland_prepare.data = this;
	ev_prepare_start(loop_.get(), &this->wayland_prepare);

	ev_signal_init(&this->sigint_handler, on_sigint_cb, SIGINT);
	this->sigint_handler.data = this;
	ev_signal_start(loop_.get(), &this->sigint_handler);

	wl_display_init_shm(display_.get());

	// XXX: For debugging only!
	keybind_ctx_.get_default().add_bind(debug_escape_bind);

	ev_prepare_init(&this->idle_task_executor, &Compositor::execute_idle_task);
	this->idle_task_executor.data = this;

	add_uncancellable_idle_task([this]() { this->lua_.load_user_init(); });
}

bool
Compositor::find_usable_socket()
{
	char dpyname[16];
	for (size_t i = 0; i <= 32; ++i) {
		snprintf(dpyname, sizeof(dpyname), "wayland-%zu", i);
		if (wl_display_add_socket(display_.get(), dpyname) >= 0) {
			dpyname_ = dpyname;
			return true;
		}
	}
	return false;
}

void
Compositor::add_uncancellable_idle_task(std::function<void(void)> &&func)
{
	idle_task_queue.add_uncancellable_callback(std::move(func));
	if (!ev_is_active(&this->idle_task_executor)) {
		ev_prepare_start(loop_.get(), &this->idle_task_executor);
	}
}

util::EventToken
Compositor::add_idle_task(std::function<void(void)> &&func)
{
	auto token = idle_task_queue.add_callback(std::move(func));
	if (!ev_is_active(&this->idle_task_executor)) {
		ev_prepare_start(loop_.get(), &this->idle_task_executor);
	}
	return token;
}

void
Compositor::execute_idle_task(EV_P_ ev_prepare *w, int revents)
{
	Compositor *self = static_cast<Compositor *>(w->data);
	if (self->idle_task_queue.has_callbacks()) {
		self->idle_task_queue.run_callback();
	}

	if (!self->idle_task_queue.has_callbacks()) {
		ev_prepare_stop(EV_A, w);
	}
}

int
Compositor::lua_get_default_keymap(struct lua_State *L)
{
	return lua::Context::wrap_function(L, [](lua::Context::LuaWrapper &L) {
		auto self = L.get_userdata<Compositor>(lua_upvalueindex(1));

		self->keybind_ctx_.push_default_keymap();

		return 1;
	});
}

int
Compositor::lua_get_keymaps_table(struct lua_State *L)
{
	return lua::Context::wrap_function(L, [](lua::Context::LuaWrapper &L) {
		auto self = L.get_userdata<Compositor>(lua_upvalueindex(1));

		self->keybind_ctx_.push_keymaps_table();

		return 1;
	});
}

int
Compositor::lua_execute(struct lua_State *L)
{
	return lua::Context::wrap_function(L, [](lua::Context::LuaWrapper &L) {
		auto cmdline = L.get<std::string>(1);
		pid_t pid = fork();
		if (pid == -1) {
			throw std::system_error(errno, std::generic_category(), "Could not spawn process");
		}
		if (pid == 0) {
			// We're in the child process.
			// Spawn the command specified as argument.
			if (execl("/bin/sh", "/bin/sh", "-c", cmdline.c_str(), nullptr) < 0) {
				_exit(EXIT_FAILURE);
			}
		}

		// Let's provide the caller the PID, so they can use that if
		// they're interested.
		L.push(pid);
		return 1;
	});
}

void
Compositor::create_compositor_global()
{
	lua::Context &ctx = lua_.ctx();

	ctx.create_unowned_userdata(this);

	ctx.new_metatable("orbycomp::Compositor");

	ctx.new_table();
	ctx.push_value(-3);
	ctx.register_lib(Compositor::compositor_reg, 1);

	shell_.push_lua_global();
	ctx.set_table_field("shell", -2);

	ctx.set_table_field("__index", -2);

	ctx.set_metatable(-2);

	ctx.set_global("orbycomp");
}

void
Compositor::run()
{
	ev_run(loop_.get());
}

void
Compositor::dispatch_wayland_events()
{
	struct wl_event_loop *loop = wl_display_get_event_loop(display_.get());
	wl_event_loop_dispatch(loop, 0);
}

void
Compositor::prepare_wayland()
{
	wl_display_flush_clients(display_.get());
}

Seat *
Compositor::get_seat(const std::string &name)
{
	auto existing_seat = seats_.find(name);
	if (existing_seat != seats_.end()) {
		return existing_seat->second;
	}

	const auto &[pair, _] = seats_.try_emplace(name, new Seat(name, this));

	seat_added_signal(pair->second);

	return pair->second;
}

Seat *
Compositor::current_seat()
{
	if (seats_.size() == 0) {
		return nullptr;
	}

	// TODO: Add better heuristic for getting the curernt seat
	return seats_.begin()->second;
}

void
Compositor::set_active(bool active)
{
	active_ = active;
	activity_change_signal(active_);
}

void
Compositor::schedule_full_repaint()
{
	// We rerender everything after we become active again
	// so we don't need to schedule.
	if (!active_) {
		return;
	}

	for (auto *output : outputs_) {
		scheduled_outputs_.insert(output);
	}

	if (repaint_idle_token_.active()) {
		return;
	}
	repaint_idle_token_ = add_idle_task([this]() { this->repaint_scheduled_outputs(); });
}

void
Compositor::repaint_scheduled_outputs()
{
	for (auto *output : scheduled_outputs_) {
		backend_->repaint_output(output);
	}

	scheduled_outputs_.clear();
}

Compositor::~Compositor()
{
	ev_io_stop(loop_.get(), &this->wayland_watcher);
	ev_prepare_stop(loop_.get(), &this->wayland_prepare);
	ev_signal_stop(loop_.get(), &this->sigint_handler);

	// Destruct seats, if any
	for (auto &[_, seat] : seats_) {
		delete seat;
	}
}

static void
on_sigint_cb(EV_P_ ev_signal *w, int revents)
{
	// We need to close the event loop so we can quit
	// gracefully.
	ev_break(EV_A_ EVBREAK_ALL);
}

void
wayland_io_cb(EV_P_ ev_io *w, int revents)
{
	orbycomp::Compositor *compositor = static_cast<orbycomp::Compositor *>(w->data);
	compositor->dispatch_wayland_events();
}

void
wayland_prepare_cb(EV_P_ ev_prepare *w, int revents)
{
	orbycomp::Compositor *compositor = static_cast<orbycomp::Compositor *>(w->data);
	compositor->prepare_wayland();
}

void
Compositor::on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	struct wl_resource *resource = wl_resource_create(client, &wl_compositor_interface, version, id);
	if (!resource) {
		wl_client_post_no_memory(client);
		return;
	}

	wl_resource_set_implementation(resource, &Compositor::iface, data, nullptr);
}

void
Compositor::create_surface(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
	Compositor *self = static_cast<Compositor *>(wl_resource_get_user_data(resource));

	try {
		Surface::create_surface(client, id, self);
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

static void
on_region_resource_destroy(struct wl_resource *resource)
{
	Region *region = static_cast<Region *>(wl_resource_get_user_data(resource));
	delete region;
}

static void
region_destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

static void
region_add(struct wl_client *client,
	   struct wl_resource *resource,
	   int32_t x,
	   int32_t y,
	   int32_t width,
	   int32_t height)
{
	Region *region = static_cast<Region *>(wl_resource_get_user_data(resource));

	try {
		*(region) += Region::Rect::from_xy_width_height(x, y, width, height);
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

static void
region_subtract(struct wl_client *client,
		struct wl_resource *resource,
		int32_t x,
		int32_t y,
		int32_t width,
		int32_t height)
{
	Region *region = static_cast<Region *>(wl_resource_get_user_data(resource));

	try {
		*(region) -= Region::Rect::from_xy_width_height(x, y, width, height);
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

static constexpr struct wl_region_interface region_iface = {
    region_destroy,
    region_add,
    region_subtract,
};

void
Compositor::create_region(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> new_resource{
	    wl_resource_create(client, &wl_region_interface, 1, id), wl_resource_destroy};
	if (!new_resource) {
		wl_resource_post_no_memory(resource);
		return;
	}

	try {
		Region *region = new Region();
		wl_resource_set_implementation(new_resource.get(), &region_iface, region,
					       on_region_resource_destroy);

		new_resource.release();
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Compositor::on_subcompositor_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	struct wl_resource *resource = wl_resource_create(client, &wl_subcompositor_interface, version, id);
	if (!resource) {
		wl_client_post_no_memory(client);
		return;
	}

	wl_resource_set_implementation(resource, &Compositor::subcomp_iface, data, nullptr);
}

void
Compositor::subcompositor_destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Compositor::subcompositor_get_subsurface(struct wl_client *client,
					 struct wl_resource *resource,
					 uint32_t id,
					 struct wl_resource *surface,
					 struct wl_resource *parent)
{
	if (surface == parent) {
		wl_resource_post_error(resource, WL_SUBCOMPOSITOR_ERROR_BAD_SURFACE,
				       "surface cannot be its own parent");
		return;
	}

	Surface *surf = static_cast<Surface *>(wl_resource_get_user_data(surface));
	Surface *p_surf = static_cast<Surface *>(wl_resource_get_user_data(parent));

	new SubSurface(client, surf, p_surf, id);
}

} // namespace orbycomp
