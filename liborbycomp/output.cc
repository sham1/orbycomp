/*
 * output.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/output.hh>
#include <stdexcept>

namespace orbycomp
{

using Mode = Output::Mode;

Output::Output(Compositor *compositor,
	       int32_t x,
	       int32_t y,
	       std::vector<Mode> &&modes,
	       int32_t phys_width,
	       int32_t phys_height,
	       enum wl_output_subpixel subpixel,
	       int32_t scale,
	       enum wl_output_transform transform,
	       const std::string &make,
	       const std::string &model)
    : compositor_{compositor}, x_{x}, y_{y}, modes_{std::move(modes)}, phys_width_{phys_width},
      phys_height_{phys_height}, subpixel_{subpixel}, scale_{scale},
      transform_{transform}, make_{make}, model_{model},
      global_{wl_global_create(compositor_->display(), &wl_output_interface, 3, this, Output::on_global_bind),
	      wl_global_destroy}
{
	if (!global_) {
		throw std::runtime_error("Could not create output global");
	}
	wl_list_init(&resource_list);
}

static void remove_from_list(struct wl_resource *resource);

void
Output::on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	Output *self = static_cast<Output *>(data);

	struct wl_resource *resource = wl_resource_create(client, &wl_output_interface, version, id);
	if (!resource) {
		wl_client_post_no_memory(client);
		return;
	}

	wl_resource_set_implementation(resource, &Output::iface, self, remove_from_list);

	wl_list_insert(self->resource_list.prev, wl_resource_get_link(resource));

	wl_output_send_geometry(resource, self->x_, self->y_, self->phys_width_, self->phys_height_,
				self->subpixel_, self->make_.c_str(), self->model_.c_str(), self->transform_);

	wl_output_send_scale(resource, self->scale_);

	for (const auto &mode : self->modes_) {
		wl_output_send_mode(resource, mode.flags_, mode.width_, mode.height_, mode.refresh_);
	}

	wl_output_send_done(resource);
}

Output::~Output()
{
	struct wl_resource *resource, *tmp;
	wl_resource_for_each_safe(resource, tmp, &resource_list) { wl_resource_destroy(resource); }
}

const Output::Mode &
Output::current_mode() const
{
	size_t current_mode = 0;
	for (size_t i = 0; i < modes_.size(); ++i) {
		const Mode &mode = modes_[i];
		if (mode.flags_ & WL_OUTPUT_MODE_CURRENT) {
			current_mode = i;
			break;
		}
	}

	return modes_[current_mode];
}

static void
remove_from_list(struct wl_resource *resource)
{
	wl_list_remove(wl_resource_get_link(resource));
}

void
Output::wl_output_release(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

} // namespace orbycomp
