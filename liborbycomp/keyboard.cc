/*
 * keyboard.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <iostream>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/keyboard.hh>
#include <orbycomp/base/seat.hh>
#include <stdexcept>
#include <unistd.h>

namespace orbycomp
{

Keyboard::Keyboard(Seat *seat)
    : seat_{seat}, context_{this}, keymap_{this}, state_{this}, shift_mod{xkb_keymap_mod_get_index(
								    keymap_.get(), XKB_MOD_NAME_SHIFT)},
      control_mod{xkb_keymap_mod_get_index(keymap_.get(), XKB_MOD_NAME_CTRL)},
      alt_mod{xkb_keymap_mod_get_index(keymap_.get(), XKB_MOD_NAME_ALT)}, super_mod{xkb_keymap_mod_get_index(
									      keymap_.get(),
									      XKB_MOD_NAME_LOGO)}
{
	wl_list_init(&resources_);
}

Keyboard::~Keyboard()
{
	struct wl_resource *resource, *tmp;
	wl_resource_for_each_safe(resource, tmp, &resources_) { wl_resource_destroy(resource); }
}

Keyboard::XkbContext::XkbContext(Keyboard *keyboard, enum xkb_context_flags flags)
    : keyboard_{keyboard}, context_{xkb_context_new(flags), xkb_context_unref}
{
	if (!context_) {
		throw std::runtime_error("Couldn't create keyboard context");
	}
}

Keyboard::XkbKeymap::XkbKeymap(Keyboard *keyboard)
    : keyboard_{keyboard}, map_{xkb_keymap_new_from_names(
				    keyboard_->context_.get(), nullptr, XKB_KEYMAP_COMPILE_NO_FLAGS),
				xkb_keymap_unref}
{
	if (!map_) {
		throw std::runtime_error("Couldn't load keyboard map");
	}
}

Keyboard::XkbState::XkbState(Keyboard *keyboard)
    : keyboard_{keyboard}, state_{xkb_state_new(keyboard_->keymap_.get()), xkb_state_unref}
{
	if (!state_) {
		throw std::runtime_error("Couldn't create keyboard state");
	}
}

void
Keyboard::feed_native_keypress(std::uint32_t key, Keyboard::KeyState state, uint32_t tv_msec)
{
	// TODO: Make this less Linux/evdev-specific
	std::uint32_t translated_key = key + 8;

	enum xkb_key_direction dir = state == KeyState::Press ? XKB_KEY_DOWN : XKB_KEY_UP;

	const xkb_keysym_t *syms;
	size_t nsyms = xkb_state_key_get_syms(state_.get(), translated_key, &syms);
	xkb_state_update_key(state_.get(), translated_key, dir);

	// TODO: Send key event to any focused client
	// if no keybind is found.
	bool found = false;
	if (state == KeyState::Press) {
		Modifier mods = Modifier::NoModifier;
		auto check_mod = [=](xkb_mod_index_t mod_idx) {
			return xkb_state_mod_index_is_active(
			    state_.get(), mod_idx,
			    static_cast<enum xkb_state_component>(XKB_STATE_EFFECTIVE));
		};
		if (check_mod(shift_mod)) {
			mods |= Modifier::Shift;
		}
		if (check_mod(control_mod)) {
			mods |= Modifier::Control;
		}
		if (check_mod(alt_mod)) {
			mods |= Modifier::Alt;
		}
		if (check_mod(super_mod)) {
			mods |= Modifier::Super;
		}

		for (size_t i = 0; i < nsyms; ++i) {
			using Keymap = KeybindContext::Keymap;
			xkb_keysym_t sym = syms[i];

			Compositor *compositor = seat_->compositor();
			// First, we shall go to the global binds to see if they
			// match anything.
			Keymap &global = compositor->keybind_ctx().get_global();
			for (const Keybind &bind : global) {
				if (bind.matches(mods, sym)) {
					found = true;
					bind();
					break;
				}
			}

			if (found) {
				break;
			}

			// If global map didn't have the proper bind, check the
			// current map.
			Keymap &current = compositor->keybind_ctx().get_current();
			for (const Keybind &bind : current) {
				if (bind.matches(mods, sym)) {
					found = true;
					bind();
					break;
				}
			}

			if (found) {
				break;
			}
		}
	}
	if (found) {
		return;
	}

	if (auto *surface_focus = std::get_if<SurfaceFocus>(&focus_)) {
		if (surface_focus->surface_ && surface_focus->keyboard_resource_) {
			uint32_t serial = wl_display_next_serial(seat_->compositor()->display());

			enum wl_keyboard_key_state wl_state;
			if (state == KeyState::Press) {
				wl_state = WL_KEYBOARD_KEY_STATE_PRESSED;
			}
			if (state == KeyState::Release) {
				wl_state = WL_KEYBOARD_KEY_STATE_RELEASED;
			}

			wl_keyboard_send_key(surface_focus->keyboard_resource_, serial, tv_msec, key,
					     wl_state);

			// TODO: Only send modifiers if they changed.
			uint32_t mods_serial = wl_display_next_serial(seat_->compositor()->display());

			uint32_t depressed = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_MODS_DEPRESSED));

			uint32_t latched = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_MODS_LATCHED));

			uint32_t locked = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_MODS_LOCKED));

			uint32_t group = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_LAYOUT_EFFECTIVE));

			wl_keyboard_send_modifiers(surface_focus->keyboard_resource_, mods_serial, depressed,
						   latched, locked, group);
		}
	}
}

void
Keyboard::feed_modifiers(uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked, uint32_t group)
{
	xkb_state_update_mask(state_.get(), mods_depressed, mods_latched, mods_locked, group, 0, 0);

	if (auto *surface_focus = std::get_if<SurfaceFocus>(&focus_)) {
		if (surface_focus->surface_ && surface_focus->keyboard_resource_) {
			uint32_t mods_serial = wl_display_next_serial(seat_->compositor()->display());

			uint32_t depressed = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_MODS_DEPRESSED));

			uint32_t latched = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_MODS_LATCHED));

			uint32_t locked = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_MODS_LOCKED));

			uint32_t group = xkb_state_serialize_mods(
			    state_.get(), static_cast<enum xkb_state_component>(XKB_STATE_LAYOUT_EFFECTIVE));

			wl_keyboard_send_modifiers(surface_focus->keyboard_resource_, mods_serial, depressed,
						   latched, locked, group);
		}
	}
}

void
Keyboard::create_resource(struct wl_client *client, uint32_t id, int seat_version)
{
	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> resource{
	    wl_resource_create(client, &wl_keyboard_interface, seat_version, id), wl_resource_destroy};
	if (!resource) {
		throw std::bad_alloc();
	}

	wl_resource_set_implementation(resource.get(), &Keyboard::iface, this, Keyboard::on_resource_destroy);

	char keymap_path[] = "/tmp/orbycomp-keymap.XXXXXX";
	struct FDHolder {
		int fd_{-1};

		int
		get() const
		{
			return fd_;
		}

		~FDHolder()
		{
			if (fd_ <= 0) {
				close(fd_);
			}
		}

		FDHolder(int fd) : fd_{fd}
		{
			if (fd_ < 0) {
				throw std::system_error(errno, std::generic_category(),
							"Could not allocate keymap file");
			}
		}
	} keymap_fd{mkstemp(keymap_path)};

	unlink(keymap_path);

	std::string keymap{xkb_keymap_get_as_string(keymap_.get(), XKB_KEYMAP_FORMAT_TEXT_V1)};
	ssize_t written = write(keymap_fd.get(), keymap.c_str(), keymap.size());
	if (written < 0) {
		throw std::system_error(errno, std::generic_category(), "Could not write keymap file");
	}

	wl_keyboard_send_keymap(resource.get(), WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1, keymap_fd.get(),
				keymap.size());

	wl_list_insert(resources_.prev, wl_resource_get_link(resource.get()));

	send_enter_for_surface(resource.get());

	resource.release();
}

void
Keyboard::send_enter_for_surface(struct wl_resource *resource)
{
	if (!resource) {
		return;
	}

	if (auto *focus = std::get_if<SurfaceFocus>(&focus_)) {
		if (focus->keyboard_resource_) {
			return;
		}
		if (!focus->surface_) {
			return;
		}

		struct wl_client *focus_surface_client = wl_resource_get_client(focus->surface_);
		if (wl_resource_get_client(resource) != focus_surface_client) {
			return;
		}

		focus->keyboard_resource_ = resource;
		wl_resource_add_destroy_listener(focus->keyboard_resource_,
						 &focus->on_keyboard_destroy_listener_);

		struct wl_array keys;
		wl_array_init(&keys);

		uint32_t serial = wl_display_next_serial(seat_->compositor()->display());
		wl_keyboard_send_enter(focus->keyboard_resource_, serial, focus->surface_, &keys);

		wl_array_release(&keys);
	}
}

void
Keyboard::on_resource_destroy(struct wl_resource *resource)
{
	wl_list_remove(wl_resource_get_link(resource));
}

void
Keyboard::release(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Keyboard::SurfaceFocus::on_surface_destroy_cb(struct wl_listener *listener, void *user_data)
{
	SurfaceFocus *self = util::container_of(listener, &SurfaceFocus::on_surface_destroy_listener_);

	wl_list_remove(&self->on_keyboard_destroy_listener_.link);
	wl_list_init(&self->on_keyboard_destroy_listener_.link);

	wl_list_remove(&self->on_surface_destroy_listener_.link);
	wl_list_init(&self->on_surface_destroy_listener_.link);

	self->keyboard_->focus_.emplace<0>();
}

void
Keyboard::SurfaceFocus::on_keyboard_destroy_cb(struct wl_listener *listener, void *user_data)
{
	SurfaceFocus *self = util::container_of(listener, &SurfaceFocus::on_keyboard_destroy_listener_);

	wl_list_remove(&self->on_keyboard_destroy_listener_.link);
	wl_list_init(&self->on_keyboard_destroy_listener_.link);

	if (self->surface_) {
		uint32_t serial = wl_display_next_serial(self->keyboard_->seat_->compositor()->display());
		wl_keyboard_send_leave(self->keyboard_resource_, serial, self->surface_);
	}

	self->keyboard_resource_ = nullptr;
}

Keyboard::SurfaceFocus::SurfaceFocus(struct wl_resource *surface, Keyboard *keyboard)
    : keyboard_{keyboard}, surface_{surface}, keyboard_resource_{find_keyboard_resource()}
{
	wl_list_init(&on_keyboard_destroy_listener_.link);
	wl_list_init(&on_surface_destroy_listener_.link);

	on_keyboard_destroy_listener_.notify = SurfaceFocus::on_keyboard_destroy_cb;
	on_surface_destroy_listener_.notify = SurfaceFocus::on_surface_destroy_cb;

	if (surface_) {
		wl_resource_add_destroy_listener(surface_, &on_surface_destroy_listener_);
	}

	if (keyboard_resource_) {
		wl_resource_add_destroy_listener(keyboard_resource_, &on_keyboard_destroy_listener_);
	}

	if (surface_ && keyboard_resource_) {
		uint32_t serial = wl_display_next_serial(keyboard_->seat_->compositor()->display());

		struct wl_array keys;
		wl_array_init(&keys);

		wl_keyboard_send_enter(keyboard_resource_, serial, surface_, &keys);

		wl_array_release(&keys);
	}
}

struct wl_resource *
Keyboard::SurfaceFocus::find_keyboard_resource()
{
	if (!surface_) {
		return nullptr;
	}

	struct wl_client *client = wl_resource_get_client(surface_);

	struct wl_resource *resource{nullptr};
	bool found{false};
	wl_resource_for_each(resource, &keyboard_->resources_)
	{
		if (wl_resource_get_client(resource) == client) {
			found = true;
			break;
		}
	}

	return found ? resource : nullptr;
}

Keyboard::SurfaceFocus::~SurfaceFocus()
{
	if (keyboard_resource_) {
		uint32_t serial = wl_display_next_serial(keyboard_->seat_->compositor()->display());
		wl_keyboard_send_leave(keyboard_resource_, serial, surface_);
	}

	wl_list_remove(&on_keyboard_destroy_listener_.link);
	wl_list_remove(&on_surface_destroy_listener_.link);
}

void
Keyboard::make_surface_focus(struct wl_resource *surface)
{
	focus_.emplace<1>(surface, this);
}

void
Keyboard::remove_surface_focus(struct wl_resource *surface)
{
	if (auto *sf = std::get_if<SurfaceFocus>(&focus_)) {
		if (sf->surface_ == surface) {
			focus_.emplace<0>();
		}
	}
}

Surface *
Keyboard::get_focus_surface()
{
	if (auto *surface_focus = std::get_if<SurfaceFocus>(&focus_)) {
		if (surface_focus->surface_) {
			Surface *surface =
			    static_cast<Surface *>(wl_resource_get_user_data(surface_focus->surface_));

			return surface;
		}
	}

	return nullptr;
}

} // namespace orbycomp
