/*
 * xdg-shell.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/surface.hh>
#include <orbycomp/xdg-shell.hh>

namespace orbycomp
{

namespace shell
{

namespace xdg
{

Shell::Shell(orbycomp::shell::Shell *shell)
    : shell_{shell}, global_{wl_global_create(shell_->compositor()->display(),
					      &xdg_wm_base_interface,
					      3,
					      this,
					      Shell::on_global_bind),
			     &wl_global_destroy}
{
}

void
Shell::on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	Shell *self = static_cast<Shell *>(data);

	// We need to do this sort of stuff to make sure that
	// if the allocation of ProtocolBase fails, that we don't leak
	// the memory.
	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> resource{
	    wl_resource_create(client, &xdg_wm_base_interface, version, id), wl_resource_destroy};
	if (!resource) {
		wl_client_post_no_memory(client);
		return;
	}

	try {
		ProtocolBase *base = new ProtocolBase(self, resource.get());
		wl_resource_set_implementation(resource.get(), &ProtocolBase::iface, base,
					       ProtocolBase::on_resource_destroy);

		// And release resource here so we don't immediately destroy it.
		resource.release();
	} catch (...) {
		wl_client_post_no_memory(client);
	}
}

void
ProtocolBase::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
ProtocolBase::create_positioner(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> new_resource{
	    wl_resource_create(client, &xdg_positioner_interface, 3, id), wl_resource_destroy};

	if (!new_resource) {
		wl_client_post_no_memory(client);
		return;
	}

	try {
		Positioner *positioner = new Positioner(new_resource.get());
		wl_resource_set_implementation(new_resource.get(), &Positioner::iface, positioner,
					       Positioner::on_resource_destroy);

		new_resource.release();
	} catch (std::bad_alloc &) {
		wl_client_post_no_memory(client);
	}
}

void
ProtocolBase::get_xdg_surface(struct wl_client *client,
			      struct wl_resource *resource,
			      uint32_t id,
			      struct wl_resource *surface)
{
	ProtocolBase *base = static_cast<ProtocolBase *>(wl_resource_get_user_data(resource));

	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> new_resource{
	    wl_resource_create(client, &xdg_surface_interface, 3, id), wl_resource_destroy};

	if (!new_resource) {
		wl_client_post_no_memory(client);
		return;
	}

	try {
		Surface *new_surface = new Surface(base, new_resource.get(), surface);
		wl_resource_set_implementation(new_resource.get(), &Surface::iface, new_surface,
					       Surface::on_resource_destroy);
		new_surface->configure_serial_ = wl_display_next_serial(wl_client_get_display(client));

		xdg_surface_send_configure(new_resource.get(), new_surface->configure_serial_);
		new_resource.release();
	} catch (std::bad_alloc &) {
		wl_client_post_no_memory(client);
	}
}

void
ProtocolBase::pong(struct wl_client *client, struct wl_resource *resource, uint32_t serial)
{
	// TODO: Implement
}

void
ProtocolBase::on_resource_destroy(struct wl_resource *resource)
{
	ProtocolBase *self = static_cast<ProtocolBase *>(wl_resource_get_user_data(resource));
	delete self;
}

Surface::Surface(ProtocolBase *base, struct wl_resource *resource, struct wl_resource *surface)
    : base_{base}, compositor_{base_->shell_->shell_->compositor()}, resource_{resource},
      surface_resource_{surface}, surface_{static_cast<orbycomp::Surface *>(
				      wl_resource_get_user_data(surface_resource_))}
{
	surface_->on_commit_signal_.connect([this](orbycomp::Surface *surface) {
		if (this->pending_geometry_) {
			surface->geometry_ = this->pending_geometry_;
			this->pending_geometry_.reset();
		}

		if (!mapped_) {
			if (role_.index() != 0 && surface_->renderable() != nullptr) {
				surface_->renderable()->layer() = Renderable::Layer::Window;

				surface_->renderable()->update();
				mapped_ = true;

				if (auto toplevel = std::get_if<Toplevel *>(&role_)) {
					base_->shell_->shell_->add_window(*toplevel);
				}
			}
		}
	});

	on_base_destroy_.notify = Surface::on_base_destroy_cb;
	wl_resource_add_destroy_listener(base_->resource_, &on_base_destroy_);
}

void
Surface::on_base_destroy_cb(struct wl_listener *listener, void *user_data)
{
	Surface *self = util::container_of(listener, &Surface::on_base_destroy_);
	wl_resource_destroy(self->resource_);
}

void
Surface::on_resource_destroy(struct wl_resource *resource)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	wl_list_remove(&self->on_base_destroy_.link);

	delete self;
}

void
Surface::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Surface::get_toplevel(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
	Surface *surface = static_cast<Surface *>(wl_resource_get_user_data(resource));

	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> new_resource{
	    wl_resource_create(client, &xdg_toplevel_interface, 3, id), wl_resource_destroy};

	if (!new_resource) {
		wl_client_post_no_memory(client);
		return;
	}

	try {
		Toplevel *new_toplevel = new Toplevel(surface, new_resource.get());
		wl_resource_set_implementation(new_resource.get(), &Toplevel::iface, new_toplevel,
					       Toplevel::on_resource_destroy);

		surface->role_.emplace<1>(new_toplevel);

		new_resource.release();
	} catch (std::bad_alloc &) {
		wl_client_post_no_memory(client);
	}
}

void
Surface::get_popup(struct wl_client *client,
		   struct wl_resource *resource,
		   uint32_t id,
		   struct wl_resource *parent,
		   struct wl_resource *positioner)
{
	Surface *surface = static_cast<Surface *>(wl_resource_get_user_data(resource));
	Positioner *positioner_ = static_cast<Positioner *>(wl_resource_get_user_data(positioner));
	if (!positioner_->is_valid()) {
		wl_resource_post_error(resource, XDG_WM_BASE_ERROR_INVALID_POSITIONER,
				       "positioner object is not complete");
		return;
	}

	auto geometry = positioner_->get_geometry();

	Surface *parent_surface{nullptr};
	if (parent) {
		parent_surface = static_cast<Surface *>(wl_resource_get_user_data(parent));
	}

	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> new_resource{
	    wl_resource_create(client, &xdg_popup_interface, 3, id), wl_resource_destroy};

	if (!new_resource) {
		wl_client_post_no_memory(client);
		return;
	}

	try {
		Popup *new_popup = new Popup(surface, new_resource.get(), parent_surface);
		wl_resource_set_implementation(new_resource.get(), &Popup::iface, new_popup,
					       Popup::on_resource_destroy);

		surface->role_.emplace<2>(new_popup);

		xdg_popup_send_configure(new_resource.get(), geometry.x_, geometry.y_, geometry.width_,
					 geometry.height_);

		surface->surface_->x() = geometry.x_;
		surface->surface_->y() = geometry.y_;

		new_resource.release();
	} catch (std::bad_alloc &) {
		wl_client_post_no_memory(client);
	}
}

void
Surface::set_window_geometry(struct wl_client *client,
			     struct wl_resource *resource,
			     int32_t x,
			     int32_t y,
			     int32_t width,
			     int32_t height)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));
	self->pending_geometry_.emplace(x, y, width, height);
}

void
Surface::ack_configure(struct wl_client *client, struct wl_resource *resource, uint32_t serial)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));
	// TODO: Do this actually properly
	if (serial > self->configure_serial_) {
		wl_resource_post_error(self->base_->resource_, XDG_WM_BASE_ERROR_INVALID_SURFACE_STATE,
				       "Wrong configure serial: %u", serial);
		return;
	}
	self->configure_serial_ = wl_display_next_serial(wl_client_get_display(client));
}

void
Surface::map()
{
	// TODO: implement
}

void
Surface::unmap()
{
	// TODO: implement
}

orbycomp::shell::Toplevel *
Surface::toplevel()
{
	return role_.index() == 1 ? std::get<1>(role_) : nullptr;
}

orbycomp::shell::Popup *
Surface::popup()
{
	return role_.index() == 2 ? std::get<2>(role_) : nullptr;
}

Surface::~Surface() {}

Toplevel::Toplevel(Surface *surface, struct wl_resource *resource) : surface_{surface}, resource_{resource}
{
	send_configuration(0, 0);

	on_surface_destroy_.notify = Toplevel::on_surface_destroy_cb;
	wl_resource_add_destroy_listener(surface->resource_, &on_surface_destroy_);
}

void
Toplevel::on_surface_destroy_cb(struct wl_listener *listener, void *user_data)
{
	Toplevel *self = util::container_of(listener, &Toplevel::on_surface_destroy_);

	wl_resource_destroy(self->resource_);
}

void
Toplevel::on_resource_destroy(struct wl_resource *resource)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));

	wl_list_remove(&self->on_surface_destroy_.link);

	Surface *surface = self->surface_;

	delete self;
	surface->role_.emplace<0>();
}

void
Toplevel::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Toplevel::set_parent(struct wl_client *client, struct wl_resource *resource, struct wl_resource *parent)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Toplevel::set_title(struct wl_client *client, struct wl_resource *resource, const char *title)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));

	try {
		self->title_ = title;
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Toplevel::set_app_id(struct wl_client *client, struct wl_resource *resource, const char *app_id)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));

	try {
		self->app_id_ = app_id;
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Toplevel::show_window_menu(struct wl_client *client,
			   struct wl_resource *resource,
			   struct wl_resource *seat,
			   uint32_t serial,
			   int32_t x,
			   int32_t y)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Toplevel::move(struct wl_client *client,
	       struct wl_resource *resource,
	       struct wl_resource *seat,
	       uint32_t serial)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));

	// TODO: Implement
}

void
Toplevel::resize(struct wl_client *client,
		 struct wl_resource *resource,
		 struct wl_resource *seat,
		 uint32_t serial,
		 uint32_t edges)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));

	// TODO: Implement
}

void
Toplevel::set_max_size(struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));

	self->max_size_.width = width;
	self->max_size_.height = height;
}

void
Toplevel::set_min_size(struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));

	self->min_size_.width = width;
	self->min_size_.height = height;
}

void
Toplevel::set_maximized(struct wl_client *client, struct wl_resource *resource)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Toplevel::unset_maximized(struct wl_client *client, struct wl_resource *resource)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Toplevel::set_fullscreen(struct wl_client *client, struct wl_resource *resource, struct wl_resource *output)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Toplevel::unset_fullscreen(struct wl_client *client, struct wl_resource *resource)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Toplevel::set_minimized(struct wl_client *client, struct wl_resource *resource)
{
	Toplevel *self = static_cast<Toplevel *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Toplevel::set_size(int32_t width, int32_t height)
{
	if (!pending_config_) {
		pending_config_.emplace();
		pending_config_->new_active = is_active_;
		pending_config_->new_maximized = is_maximized_;

		pending_config_send_token =
		    surface_->compositor_->add_idle_task([this]() { this->apply_pending(); });
	}
	pending_config_->new_width = width;
	pending_config_->new_height = height;
}

void
Toplevel::send_configuration(int32_t width, int32_t height)
{
	std::vector<enum xdg_toplevel_state> states{};
	try {
		if (is_active_) {
			states.emplace_back(XDG_TOPLEVEL_STATE_ACTIVATED);
		}

		if (is_maximized_) {
			states.emplace_back(XDG_TOPLEVEL_STATE_MAXIMIZED);
		}

		if (is_resizing_) {
			states.emplace_back(XDG_TOPLEVEL_STATE_RESIZING);
		}

		struct wl_array state_arr;
		wl_array_init(&state_arr);

		enum xdg_toplevel_state *array_head = static_cast<enum xdg_toplevel_state *>(
		    wl_array_add(&state_arr, states.size() * sizeof(*states.data())));
		if (!array_head) {
			wl_array_release(&state_arr);
			throw std::bad_alloc();
		}

		std::copy(states.begin(), states.end(), array_head);

		xdg_toplevel_send_configure(resource_, width, height, &state_arr);

		wl_array_release(&state_arr);

	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource_);
	}
}

void
Toplevel::set_maximized(bool maximized)
{
	if (!pending_config_) {
		auto &geom = surface_->surface_->geometry_;

		pending_config_.emplace();

		if (geom) {
			pending_config_->new_width = geom->width_;
			pending_config_->new_height = geom->height_;
		} else {
			pending_config_->new_width = 0;
			pending_config_->new_height = 0;
		}

		pending_config_->new_active = is_active_;

		pending_config_send_token =
		    surface_->compositor_->add_idle_task([this]() { this->apply_pending(); });
	}
	pending_config_->new_maximized = maximized;
}

void
Toplevel::set_activity(bool active)
{
	if (!pending_config_) {
		auto &geom = surface_->surface_->geometry_;

		pending_config_.emplace();

		if (geom) {
			pending_config_->new_width = geom->width_;
			pending_config_->new_height = geom->height_;
		} else {
			pending_config_->new_width = 0;
			pending_config_->new_height = 0;
		}

		pending_config_->new_maximized = is_maximized_;

		pending_config_send_token =
		    surface_->compositor_->add_idle_task([this]() { this->apply_pending(); });
	}
	pending_config_->new_active = active;
}

void
Toplevel::apply_pending()
{
	is_active_ = pending_config_->new_active;
	is_maximized_ = pending_config_->new_maximized;
	send_configuration(pending_config_->new_width, pending_config_->new_height);

	pending_config_.reset();
}

void
Toplevel::close()
{
	xdg_toplevel_send_close(resource_);
}

Toplevel::~Toplevel() { surface_->base_->shell_->shell_->remove_window(this); }

Popup::Popup(Surface *surface, struct wl_resource *resource, Surface *parent)
    : surface_{surface}, resource_{resource}, parent_{parent}
{
	on_surface_destroy_.notify = Popup::on_surface_destroy_cb;
	wl_resource_add_destroy_listener(surface->resource_, &on_surface_destroy_);
}

void
Popup::on_surface_destroy_cb(struct wl_listener *listener, void *user_data)
{
	Popup *self = util::container_of(listener, &Popup::on_surface_destroy_);

	wl_resource_destroy(self->resource_);
}

void
Popup::on_resource_destroy(struct wl_resource *resource)
{
	Popup *self = static_cast<Popup *>(wl_resource_get_user_data(resource));

	wl_list_remove(&self->on_surface_destroy_.link);

	self->surface_->role_.emplace<0>();
	delete self;
}

void
Popup::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Popup::grab(struct wl_client *client, struct wl_resource *resource, struct wl_resource *seat, uint32_t serial)
{
	Popup *self = static_cast<Popup *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Popup::reposition(struct wl_client *client,
		  struct wl_resource *resource,
		  struct wl_resource *positioner,
		  uint32_t token)
{
	Popup *self = static_cast<Popup *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

Popup::~Popup() { surface_->base_->shell_->shell_->remove_popup(this); }

void
Positioner::on_resource_destroy(struct wl_resource *resource)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));

	delete self;
}

void
Positioner::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Positioner::set_size(struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));

	if (width < 0 || height < 0) {
		wl_resource_post_error(resource, XDG_POSITIONER_ERROR_INVALID_INPUT,
				       "Width and height must be non-negative");
		return;
	}

	self->width_ = width;
	self->height_ = height;
}

void
Positioner::set_anchor_rect(struct wl_client *client,
			    struct wl_resource *resource,
			    int32_t x,
			    int32_t y,
			    int32_t width,
			    int32_t height)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));

	if (width < 0 || height < 0) {
		wl_resource_post_error(resource, XDG_POSITIONER_ERROR_INVALID_INPUT,
				       "Width and height must be non-negative");
		return;
	}

	self->anchor_rect_.emplace(x, y, width, height);
}

void
Positioner::set_anchor(struct wl_client *client, struct wl_resource *resource, uint32_t anchor)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));

	self->anchor_ = static_cast<enum xdg_positioner_anchor>(anchor);
}

void
Positioner::set_gravity(struct wl_client *client, struct wl_resource *resource, uint32_t gravity)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));

	self->gravity_ = static_cast<enum xdg_positioner_gravity>(gravity);
}

void
Positioner::set_constraint_adjustment(struct wl_client *client,
				      struct wl_resource *resource,
				      uint32_t constraint_adjustment)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));

	self->adjust_ = static_cast<enum xdg_positioner_constraint_adjustment>(constraint_adjustment);
}

void
Positioner::set_offset(struct wl_client *client, struct wl_resource *resource, int32_t x, int32_t y)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));
	self->off_x_ = x;
	self->off_y_ = y;
}

void
Positioner::set_reactive(struct wl_client *client, struct wl_resource *resource)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Positioner::set_parent_size(struct wl_client *client,
			    struct wl_resource *resource,
			    int32_t parent_width,
			    int32_t parent_height)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

void
Positioner::set_parent_configure(struct wl_client *client, struct wl_resource *resource, uint32_t serial)
{
	Positioner *self = static_cast<Positioner *>(wl_resource_get_user_data(resource));
	// TODO: Implement
}

orbycomp::Surface::Geometry
Positioner::get_geometry() const
{
	orbycomp::Surface::Geometry geometry{off_x_, off_y_, width_, height_};

	switch (anchor_) {
	case XDG_POSITIONER_ANCHOR_TOP:
	case XDG_POSITIONER_ANCHOR_TOP_LEFT:
	case XDG_POSITIONER_ANCHOR_TOP_RIGHT:
		geometry.y_ += anchor_rect_->y_;
		break;
	case XDG_POSITIONER_ANCHOR_BOTTOM:
	case XDG_POSITIONER_ANCHOR_BOTTOM_LEFT:
	case XDG_POSITIONER_ANCHOR_BOTTOM_RIGHT:
		geometry.y_ += anchor_rect_->y_ + anchor_rect_->height_;
		break;
	default:
		geometry.y_ += anchor_rect_->y_ + anchor_rect_->height_ / 2;
	}

	switch (anchor_) {
	case XDG_POSITIONER_ANCHOR_LEFT:
	case XDG_POSITIONER_ANCHOR_TOP_LEFT:
	case XDG_POSITIONER_ANCHOR_BOTTOM_LEFT:
		geometry.x_ += anchor_rect_->x_;
		break;
	case XDG_POSITIONER_ANCHOR_RIGHT:
	case XDG_POSITIONER_ANCHOR_TOP_RIGHT:
	case XDG_POSITIONER_ANCHOR_BOTTOM_RIGHT:
		geometry.x_ += anchor_rect_->x_ + anchor_rect_->width_;
		break;
	default:
		geometry.y_ += anchor_rect_->x_ + anchor_rect_->width_ / 2;
	}

	switch (gravity_) {
	case XDG_POSITIONER_GRAVITY_TOP:
	case XDG_POSITIONER_GRAVITY_TOP_LEFT:
	case XDG_POSITIONER_GRAVITY_TOP_RIGHT:
		geometry.y_ -= geometry.height_;
		break;
	case XDG_POSITIONER_GRAVITY_BOTTOM:
	case XDG_POSITIONER_GRAVITY_BOTTOM_LEFT:
	case XDG_POSITIONER_GRAVITY_BOTTOM_RIGHT:
		// geometry.y_ = geometry.y_;
		break;
	default:
		geometry.y_ -= geometry.height_ / 2;
	}

	switch (gravity_) {
	case XDG_POSITIONER_GRAVITY_LEFT:
	case XDG_POSITIONER_GRAVITY_TOP_LEFT:
	case XDG_POSITIONER_GRAVITY_BOTTOM_LEFT:
		geometry.x_ -= geometry.width_;
		break;
	case XDG_POSITIONER_GRAVITY_RIGHT:
	case XDG_POSITIONER_GRAVITY_TOP_RIGHT:
	case XDG_POSITIONER_GRAVITY_BOTTOM_RIGHT:
		// geometry.x_ = geometry.x_;
		break;
	default:
		geometry.x_ -= geometry.width_ / 2;
	}

	// TODO: use constraints

	return geometry;
}

Positioner::~Positioner() {}

} // namespace xdg

} // namespace shell

} // namespace orbycomp
