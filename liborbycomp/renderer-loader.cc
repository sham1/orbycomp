/*
 * renderer-loader.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp-conf.h>
#include <orbycomp/renderer-loader.hh>
#include <sstream>
#include <stdexcept>

namespace orbycomp
{

RendererHandle
load_renderer(const std::string &renderer_name, RendererOptions &opts)
{
	using RendererInit = Renderer *(*) (RendererOptions &);

	std::unique_ptr<void, RendererHandle::dynamic_handle_closer> renderer_handle(
	    dlopen(renderer_name.c_str(), RTLD_LAZY));

	if (renderer_handle == nullptr) {
		throw std::runtime_error(dlerror());
	}

	// Clear any existing errors.
	dlerror();

	RendererInit init = reinterpret_cast<RendererInit>(dlsym(renderer_handle.get(), "renderer_init"));

	{
		const char *error_tmp = nullptr;
		if ((error_tmp = dlerror()) != nullptr) {
			throw std::runtime_error(error_tmp);
		}
	}

	std::unique_ptr<Renderer> renderer(init(opts));

	return {std::move(renderer), std::move(renderer_handle)};
}

}; // namespace orbycomp
