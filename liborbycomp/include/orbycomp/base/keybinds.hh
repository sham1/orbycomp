/*
 * keybinds.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <iosfwd>
#include <orbycomp/lua.hh>
#include <orbycomp/util/intrusive-list.hh>
#include <string>
#include <unordered_map>
#include <xkbcommon/xkbcommon.h>

namespace orbycomp
{

class Compositor;

enum class Modifier : uint8_t {
	NoModifier = 0,
	Control = (1 << 0),
	Alt = (1 << 1),
	Shift = (1 << 2),
	Super = (1 << 3),
};

using KeyCallback = std::function<void(Modifier, xkb_keysym_t)>;

class Keybind : public util::IntrusiveListNode<Keybind>
{
public:
	bool matches(Modifier mods, xkb_keysym_t sym) const;

	Keybind(Modifier mods, xkb_keysym_t sym, KeyCallback cb);
	Keybind() = default;
	~Keybind() {}

	Keybind(Keybind &&other) = default;
	Keybind(const Keybind &) = delete;

	Keybind &operator=(Keybind &&other) = default;
	Keybind &operator=(const Keybind &other) = delete;

	inline void
	operator()() const
	{
		callback_(mods_, sym_);
	}

private:
	KeyCallback callback_;
	Modifier mods_;
	xkb_keysym_t sym_;
};

inline constexpr Modifier
operator&(const Modifier &a, const Modifier &b)
{
	uint8_t a_val = static_cast<uint8_t>(a);
	uint8_t b_val = static_cast<uint8_t>(b);
	return static_cast<Modifier>(a_val & b_val);
}

inline constexpr Modifier
operator|(const Modifier &a, const Modifier &b)
{
	uint8_t a_val = static_cast<uint8_t>(a);
	uint8_t b_val = static_cast<uint8_t>(b);
	return static_cast<Modifier>(a_val | b_val);
}

inline constexpr Modifier
operator^(const Modifier &a, const Modifier &b)
{
	uint8_t a_val = static_cast<uint8_t>(a);
	uint8_t b_val = static_cast<uint8_t>(b);
	return static_cast<Modifier>(a_val ^ b_val);
}

inline constexpr Modifier
operator~(const Modifier &mods)
{
	uint8_t mods_val = static_cast<uint8_t>(mods);
	return static_cast<Modifier>(~mods_val);
}

inline constexpr Modifier &
operator&=(Modifier &a, const Modifier &b)
{
	return a = (a & b);
}

inline constexpr Modifier &
operator|=(Modifier &a, const Modifier &b)
{
	return a = (a | b);
}

inline constexpr Modifier &
operator^=(Modifier &a, const Modifier &b)
{
	return a = (a ^ b);
}

std::ostream &operator<<(std::ostream &os, const Modifier &mods);

class KeybindContext
{
public:
	class Keymap
	{
		std::string name_;
		util::IntrusiveList<Keybind> binds_{};

		static int lua_add_keybind(struct lua_State *L);
		static constexpr luaL_Reg keymap_reg[] = {
		    {"add_keybind", Keymap::lua_add_keybind},
		    {nullptr, nullptr},
		};

		lua::Context::Ref userdata_ref_;
		lua::Context::Ref create_userdata(KeybindContext *ctx);
		lua::Context::Ref bind_table_ref_;
		lua::Context::Ref create_bind_table(KeybindContext *ctx);

		friend KeybindContext;

		Keymap(const std::string &name, KeybindContext *ctx);

		KeybindContext *ctx_;

	public:
		~Keymap() {}

		inline const std::string &
		name() const
		{
			return name_;
		}

		void add_bind(Keybind &bind);

		inline auto
		begin() const
		{
			return binds_.begin();
		}

		inline auto
		end() const
		{
			return binds_.end();
		}
	};

	KeybindContext(Compositor *compositor);

	Keymap &get(const std::string &name);

	inline Keymap &
	get_default() noexcept
	{
		return get("default");
	}

	inline Keymap &
	get_global() noexcept
	{
		return global_;
	}

	inline Keymap &
	get_current() noexcept
	{
		return *current_keymap_;
	}

	inline void
	set_current(const std::string &name)
	{
		auto &map = maps_.at(name);
		current_keymap_ = map.get();
	}

	inline void
	activate_default_map() noexcept
	{
		set_current("default");
	}

	void
	push_keymaps_table()
	{
		keymaps_ref_.get_value();
	}

	void
	push_default_keymap()
	{
		get_default().userdata_ref_.get_value();
	}

	Keymap &create_keymap(const std::string &name);

	Keymap *current_keymap_;

private:
	Compositor *compositor_;

	lua::Context::Ref create_keymaps_ref();
	lua::Context::Ref keymaps_ref_;

	std::unordered_map<std::string, std::unique_ptr<Keymap>> maps_{};
	Keymap global_;
};

class LuaKeybind
{
	friend KeybindContext;
	friend KeybindContext::Keymap;

	lua::Context::Ref callback_ref_;
	std::unique_ptr<Keybind> bind_;

	void execute_function(Modifier mods, xkb_keysym_t sym);

	static int on_gc(struct lua_State *L);
	static constexpr luaL_Reg keybind_reg[] = {
	    {"__gc", LuaKeybind::on_gc},
	    {nullptr, nullptr},
	};

public:
	LuaKeybind(Modifier mods, xkb_keysym_t sym, lua::Context::Ref cb_ref);
	~LuaKeybind();
};

} // namespace orbycomp
