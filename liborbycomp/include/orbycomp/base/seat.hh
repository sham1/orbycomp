/*
 * seat.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <optional>
#include <orbycomp/base/keyboard.hh>
#include <orbycomp/base/pointer.hh>
#include <string>
#include <wayland-server.h>

namespace orbycomp
{

class Compositor;

class Seat
{
	std::string name_;

	Compositor *compositor_;

	friend Compositor;

	std::unique_ptr<struct wl_global, decltype(&wl_global_destroy)> global_;
	struct wl_list global_resources_ {
	};

	static void on_bind_cb(struct wl_client *client, void *data, uint32_t version, uint32_t id);

	static void get_pointer(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static void get_keyboard(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static void get_touch(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static void release(struct wl_client *client, struct wl_resource *resource);

	static constexpr struct wl_seat_interface seat_iface = {
	    Seat::get_pointer,
	    Seat::get_keyboard,
	    Seat::get_touch,
	    Seat::release,
	};

	struct Capabilities {
		int keyboards{0};
		int pointers{0};
		int touch_devices{0};

		// The equality checks whether both
		// capabilities have the same caps, irrespective of how many.
		// I.e. if `this` has two keyboards but no pointers nor touch
		// devices and `other` has three keyboards but no pointers nor
		// touch devices, they are judged to be equal.
		bool operator==(const Capabilities &other) const;

		bool
		operator!=(const Capabilities &other) const
		{
			return !this->operator==(other);
		}

		enum wl_seat_capability to_cap_bits() const;
	} caps_{};

	void update_caps(const Capabilities &old_caps);

	std::optional<Keyboard> keyboard_{};
	std::optional<Pointer> pointer_{};

public:
	Seat(const std::string &name, Compositor *compositor);
	~Seat();

	const std::string &
	name() const
	{
		return name_;
	}

	void add_keyboard_capability();
	void add_pointer_capability();
	void add_touch_capability();

	void remove_keyboard_capability();
	void remove_pointer_capability();
	void remove_touch_capability();

	inline std::optional<Keyboard> &
	keyboard()
	{
		return keyboard_;
	}

	inline std::optional<Pointer> &
	pointer()
	{
		return pointer_;
	}

	inline Compositor *
	compositor()
	{
		return compositor_;
	}

	sigc::signal<void(std::optional<Keyboard> &, std::optional<Pointer> &)> on_caps_changed_signal_{};
};

}; // namespace orbycomp
