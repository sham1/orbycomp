/*
 * keyboard.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <variant>
#include <wayland-server.h>
#include <xkbcommon/xkbcommon-keysyms.h>
#include <xkbcommon/xkbcommon.h>

namespace orbycomp
{

class Seat;
class Surface;

class Keyboard
{
	Seat *seat_;

	class XkbContext
	{
		Keyboard *keyboard_;

		std::unique_ptr<struct xkb_context, decltype(&xkb_context_unref)> context_;

	public:
		XkbContext(Keyboard *keyboard, enum xkb_context_flags flags = XKB_CONTEXT_NO_FLAGS);
		~XkbContext() {}

		inline struct xkb_context *
		get()
		{
			return context_.get();
		}
	} context_;

	class XkbKeymap
	{
		Keyboard *keyboard_;
		std::unique_ptr<struct xkb_keymap, decltype(&xkb_keymap_unref)> map_;

	public:
		XkbKeymap(Keyboard *keyboard);

		~XkbKeymap() {}

		inline struct xkb_keymap *
		get()
		{
			return map_.get();
		}
	} keymap_;

	class XkbState
	{
		Keyboard *keyboard_;
		std::unique_ptr<struct xkb_state, decltype(&xkb_state_unref)> state_;

	public:
		XkbState(Keyboard *keyboard);

		~XkbState() {}

		inline struct xkb_state *
		get()
		{
			return state_.get();
		}
	} state_;

	xkb_mod_index_t shift_mod;
	xkb_mod_index_t control_mod;
	xkb_mod_index_t alt_mod;
	xkb_mod_index_t super_mod;

	// wl_resource::link
	struct wl_list resources_ {
	};

	struct SurfaceFocus {
		Keyboard *keyboard_{nullptr};

		struct wl_resource *surface_{nullptr};
		struct wl_resource *keyboard_resource_{nullptr};
		struct wl_listener on_surface_destroy_listener_ {
		};
		struct wl_listener on_keyboard_destroy_listener_ {
		};

		static void on_surface_destroy_cb(struct wl_listener *listener, void *user_data);

		static void on_keyboard_destroy_cb(struct wl_listener *listener, void *user_data);

		struct wl_resource *find_keyboard_resource();

		~SurfaceFocus();
		SurfaceFocus(struct wl_resource *surface, Keyboard *keyboard);
	};
	std::variant<std::monostate, SurfaceFocus> focus_{std::monostate{}};
	void send_enter_for_surface(struct wl_resource *resource);

public:
	Keyboard(Seat *seat);
	~Keyboard();

	enum class KeyState {
		Press,
		Release,
	};

	void feed_native_keypress(std::uint32_t key, KeyState state, uint32_t tv_msec);
	void
	feed_modifiers(uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked, uint32_t group);

	void create_resource(struct wl_client *client, uint32_t id, int seat_resource);

	static void on_resource_destroy(struct wl_resource *resource);

	static void release(struct wl_client *client, struct wl_resource *resource);

	static constexpr struct wl_keyboard_interface iface = {
	    Keyboard::release,
	};

	void make_surface_focus(struct wl_resource *surface);
	void remove_surface_focus(struct wl_resource *surface);

	/**
	 * Returns the surface which has the keyboard focus
	 * or `nullptr` if a surface does not have the focus.
	 */
	Surface *get_focus_surface();
};

} // namespace orbycomp
