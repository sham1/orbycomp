/*
 * data-device.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <orbycomp/base/seat.hh>
#include <sigc++/sigc++.h>
#include <unordered_set>
#include <wayland-server.h>

namespace orbycomp
{

class Compositor;

class DataDevice;
class DataOffer;

class DataDeviceManager
{
	Compositor *compositor_;

	std::unique_ptr<struct wl_global, decltype(&wl_global_destroy)> global_;

	struct wl_list global_resources_ {
	};

	static void on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id);

	static void create_data_source(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static void get_data_device(struct wl_client *client,
				    struct wl_resource *resource,
				    uint32_t id,
				    struct wl_resource *seat);

	static constexpr struct wl_data_device_manager_interface iface = {
	    DataDeviceManager::create_data_source,
	    DataDeviceManager::get_data_device,
	};

	std::unordered_map<Seat *, std::unique_ptr<DataDevice>> data_devices_{};

	sigc::connection seat_added_signal_connection_;

public:
	DataDeviceManager(Compositor *compositor);
	~DataDeviceManager();
};

class DataSource
{
public:
	enum class DNDAction : uint8_t {
		NoAction = 0,
		Copy = (1 << 0),
		Ask = (1 << 1),
		Move = (1 << 2),
	};

private:
	friend DataDeviceManager;
	friend DataDevice;
	friend DataOffer;

	DataDeviceManager *manager_;
	struct wl_resource *resource_;

	static void on_resource_destroy(struct wl_resource *resource);

	static void offer(struct wl_client *client, struct wl_resource *resource, const char *mime_type);

	static void destroy(struct wl_client *client, struct wl_resource *resource);

	static void set_actions(struct wl_client *client, struct wl_resource *resource, uint32_t dnd_actions);

	static constexpr struct wl_data_source_interface iface = {
	    DataSource::offer,
	    DataSource::destroy,
	    DataSource::set_actions,
	};

	std::unordered_set<std::string> mimes_{};

	DataSource(DataDeviceManager *manager, struct wl_resource *resource);
	~DataSource();

	DNDAction actions_{DNDAction::NoAction};
};

inline constexpr DataSource::DNDAction
operator&(const DataSource::DNDAction &lhs, const DataSource::DNDAction &rhs)
{
	using dnd_underlying = std::underlying_type<DataSource::DNDAction>::type;
	dnd_underlying a = static_cast<dnd_underlying>(lhs);
	dnd_underlying b = static_cast<dnd_underlying>(rhs);
	return static_cast<DataSource::DNDAction>(a & b);
}

inline constexpr DataSource::DNDAction
operator|(const DataSource::DNDAction &lhs, const DataSource::DNDAction &rhs)
{
	using dnd_underlying = std::underlying_type<DataSource::DNDAction>::type;
	dnd_underlying a = static_cast<dnd_underlying>(lhs);
	dnd_underlying b = static_cast<dnd_underlying>(rhs);
	return static_cast<DataSource::DNDAction>(a | b);
}

inline constexpr DataSource::DNDAction
operator^(const DataSource::DNDAction &lhs, const DataSource::DNDAction &rhs)
{
	using dnd_underlying = std::underlying_type<DataSource::DNDAction>::type;
	dnd_underlying a = static_cast<dnd_underlying>(lhs);
	dnd_underlying b = static_cast<dnd_underlying>(rhs);
	return static_cast<DataSource::DNDAction>(a ^ b);
}

inline constexpr DataSource::DNDAction
operator~(const DataSource::DNDAction &action)
{
	using dnd_underlying = std::underlying_type<DataSource::DNDAction>::type;
	dnd_underlying a = static_cast<dnd_underlying>(action);
	return static_cast<DataSource::DNDAction>(~a);
}

inline constexpr DataSource::DNDAction &
operator&=(DataSource::DNDAction &lhs, const DataSource::DNDAction &rhs)
{
	return lhs = (lhs & rhs);
}

inline constexpr DataSource::DNDAction &
operator|=(DataSource::DNDAction &lhs, const DataSource::DNDAction &rhs)
{
	return lhs = (lhs | rhs);
}

inline constexpr DataSource::DNDAction &
operator^=(DataSource::DNDAction &lhs, const DataSource::DNDAction &rhs)
{
	return lhs = (lhs ^ rhs);
}

class DataDevice
{
	friend DataDeviceManager;
	friend DataSource;

	DataDeviceManager *manager_;
	Seat *seat_;

	struct wl_list resources_ {
	};

	void add_resource(struct wl_resource *resource);

	static void on_resource_destroy(struct wl_resource *resource);

	static void start_drag(struct wl_client *client,
			       struct wl_resource *resource,
			       struct wl_resource *source,
			       struct wl_resource *origin,
			       struct wl_resource *icon,
			       uint32_t serial);

	static void set_selection(struct wl_client *client,
				  struct wl_resource *resource,
				  struct wl_resource *source,
				  uint32_t serial);

	static void release(struct wl_client *client, struct wl_resource *resource);

	static constexpr struct wl_data_device_interface iface = {
	    DataDevice::start_drag,
	    DataDevice::set_selection,
	    DataDevice::release,
	};

	void set_selection(DataSource *source, uint32_t serial);
	void send_selection(struct wl_client *client);

	uint32_t selection_serial_{0};

	DataSource *selection_{nullptr};

public:
	DataDevice(DataDeviceManager *manager, Seat *seat);
	~DataDevice();
};

class DataOffer
{
	friend DataDevice;

	DataDevice *device_;
	struct wl_resource *resource_;
	DataSource *source_;

	static void on_resource_destroy(struct wl_resource *resource);

	static void accept(struct wl_client *client,
			   struct wl_resource *resource,
			   uint32_t serial,
			   const char *mime_type);

	static void
	receive(struct wl_client *client, struct wl_resource *resource, const char *mime_type, int32_t fd);

	static void destroy(struct wl_client *client, struct wl_resource *resource);

	static void finish(struct wl_client *client, struct wl_resource *resource);

	static void set_actions(struct wl_client *client,
				struct wl_resource *resource,
				uint32_t dnd_actions,
				uint32_t preferred_action);

	static constexpr struct wl_data_offer_interface iface = {
	    DataOffer::accept, DataOffer::receive,     DataOffer::destroy,
	    DataOffer::finish, DataOffer::set_actions,
	};

public:
	DataOffer(DataDevice *device, struct wl_client *client, DataSource *source);
	~DataOffer();
};

} // namespace orbycomp
