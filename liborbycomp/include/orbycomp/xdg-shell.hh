/*
 * xdg-shell.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <orbycomp/base/surface.hh>
#include <wayland-server.h>
#include <xdg-shell-server-protocol.h>

#include <orbycomp/util/idle-event-queue.hh>

#include <orbycomp/base/shell.hh>

#include <string>
#include <variant>

namespace orbycomp
{

namespace shell
{

namespace xdg
{

class Surface;
class Toplevel;
class Popup;

class Shell
{
	friend Surface;
	friend Toplevel;
	friend Popup;

	orbycomp::shell::Shell *shell_;

	std::unique_ptr<struct wl_global, decltype(&wl_global_destroy)> global_;

	static void on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id);

public:
	Shell(orbycomp::shell::Shell *shell);
	~Shell() {}
};

class ProtocolBase
{
	friend Surface;
	friend Toplevel;
	friend Popup;

	Shell *shell_;
	struct wl_resource *resource_;

	static void destroy(struct wl_client *client, struct wl_resource *resource);

	static void create_positioner(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static void get_xdg_surface(struct wl_client *client,
				    struct wl_resource *resource,
				    uint32_t id,
				    struct wl_resource *surface);

	static void pong(struct wl_client *client, struct wl_resource *resource, uint32_t serial);

public:
	ProtocolBase(Shell *shell, struct wl_resource *resource) : shell_{shell}, resource_{resource} {}
	~ProtocolBase() {}

	static constexpr struct xdg_wm_base_interface iface = {
	    ProtocolBase::destroy,
	    ProtocolBase::create_positioner,
	    ProtocolBase::get_xdg_surface,
	    ProtocolBase::pong,
	};

	static void on_resource_destroy(struct wl_resource *resource);
};

class Surface : public orbycomp::shell::Surface
{
	friend ProtocolBase;
	friend Toplevel;
	friend Popup;

	ProtocolBase *base_;
	Compositor *compositor_;

	struct wl_resource *resource_;

	struct wl_resource *surface_resource_;
	orbycomp::Surface *surface_;

	bool mapped_{false};

	std::optional<orbycomp::Surface::Geometry> pending_{std::nullopt};

	static void on_base_destroy_cb(struct wl_listener *listener, void *user_data);
	struct wl_listener on_base_destroy_;

	std::variant<std::monostate, Toplevel *, Popup *> role_{std::monostate{}};

	static void on_resource_destroy(struct wl_resource *resource);

	static void destroy(struct wl_client *client, struct wl_resource *resource);

	static void get_toplevel(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static void get_popup(struct wl_client *client,
			      struct wl_resource *resource,
			      uint32_t id,
			      struct wl_resource *parent,
			      struct wl_resource *positioner);

	static void set_window_geometry(struct wl_client *client,
					struct wl_resource *resource,
					int32_t x,
					int32_t y,
					int32_t width,
					int32_t height);

	static void ack_configure(struct wl_client *client, struct wl_resource *resource, uint32_t serial);

	static constexpr struct xdg_surface_interface iface = {
	    Surface::destroy,       Surface::get_toplevel, Surface::get_popup, Surface::set_window_geometry,
	    Surface::ack_configure,
	};

	size_t configure_serial_{};

	std::optional<orbycomp::Surface::Geometry> pending_geometry_{};

public:
	Surface(ProtocolBase *base, struct wl_resource *resource, struct wl_resource *surface);
	~Surface();

	void map() override;
	void unmap() override;

	orbycomp::Surface *
	surface() override
	{
		return surface_;
	}

	orbycomp::shell::Toplevel *toplevel() override;
	orbycomp::shell::Popup *popup() override;
};

class Positioner
{
	friend ProtocolBase;
	friend Surface;
	friend Popup;

	int32_t width_{-1};
	int32_t height_{-1};

	struct Anchor {
		int32_t x_;
		int32_t y_;
		int32_t width_;
		int32_t height_;

		Anchor(int32_t x, int32_t y, int32_t width, int32_t height)
		    : x_{x}, y_{y}, width_{width}, height_{height}
		{
		}
	};
	std::optional<Anchor> anchor_rect_{};

	bool
	is_valid() const
	{
		return width_ >= 0 && height_ >= 0 && anchor_rect_;
	}

	enum xdg_positioner_anchor anchor_ { XDG_POSITIONER_ANCHOR_NONE };
	enum xdg_positioner_gravity gravity_ { XDG_POSITIONER_GRAVITY_NONE };
	enum xdg_positioner_constraint_adjustment adjust_ { XDG_POSITIONER_CONSTRAINT_ADJUSTMENT_NONE };

	int32_t off_x_{0};
	int32_t off_y_{0};

	orbycomp::Surface::Geometry get_geometry() const;

	struct wl_resource *resource_{nullptr};

	Positioner(struct wl_resource *resource) : resource_{resource} {}
	~Positioner();

	static void on_resource_destroy(struct wl_resource *resource);

	static void destroy(struct wl_client *client, struct wl_resource *resource);
	static void
	set_size(struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height);
	static void set_anchor_rect(struct wl_client *client,
				    struct wl_resource *resource,
				    int32_t x,
				    int32_t y,
				    int32_t width,
				    int32_t height);
	static void set_anchor(struct wl_client *client, struct wl_resource *resource, uint32_t anchor);
	static void set_gravity(struct wl_client *client, struct wl_resource *resource, uint32_t gravity);
	static void set_constraint_adjustment(struct wl_client *client,
					      struct wl_resource *resource,
					      uint32_t constraint_adjustment);
	static void set_offset(struct wl_client *client, struct wl_resource *resource, int32_t x, int32_t y);
	static void set_reactive(struct wl_client *client, struct wl_resource *resource);
	static void set_parent_size(struct wl_client *client,
				    struct wl_resource *resource,
				    int32_t parent_width,
				    int32_t parent_height);
	static void
	set_parent_configure(struct wl_client *client, struct wl_resource *resource, uint32_t serial);

	static constexpr struct xdg_positioner_interface iface = {
	    Positioner::destroy,         Positioner::set_size,
	    Positioner::set_anchor_rect, Positioner::set_anchor,
	    Positioner::set_gravity,     Positioner::set_constraint_adjustment,
	    Positioner::set_offset,      Positioner::set_reactive,
	    Positioner::set_parent_size, Positioner::set_parent_configure,
	};
};

class Toplevel : public orbycomp::shell::Toplevel
{
	friend Surface;

	Surface *surface_;

	std::string title_{};
	std::string app_id_{};

	struct PendingConfig {
		int32_t new_width;
		int32_t new_height;
		bool new_active;
		bool new_maximized;
	};
	std::optional<PendingConfig> pending_config_{std::nullopt};
	util::EventToken pending_config_send_token{};

	static void on_resource_destroy(struct wl_resource *resource);

	static void destroy(struct wl_client *client, struct wl_resource *resource);
	static void
	set_parent(struct wl_client *client, struct wl_resource *resource, struct wl_resource *parent);
	static void set_title(struct wl_client *client, struct wl_resource *resource, const char *title);
	static void set_app_id(struct wl_client *client, struct wl_resource *resource, const char *title);
	static void show_window_menu(struct wl_client *client,
				     struct wl_resource *resource,
				     struct wl_resource *seat,
				     uint32_t serial,
				     int32_t x,
				     int32_t y);
	static void move(struct wl_client *client,
			 struct wl_resource *resource,
			 struct wl_resource *seat,
			 uint32_t serial);
	static void resize(struct wl_client *client,
			   struct wl_resource *resource,
			   struct wl_resource *seat,
			   uint32_t serial,
			   uint32_t edges);
	static void
	set_max_size(struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height);
	static void
	set_min_size(struct wl_client *client, struct wl_resource *resource, int32_t width, int32_t height);
	static void set_maximized(struct wl_client *client, struct wl_resource *resource);
	static void unset_maximized(struct wl_client *client, struct wl_resource *resource);
	static void
	set_fullscreen(struct wl_client *client, struct wl_resource *resource, struct wl_resource *output);
	static void unset_fullscreen(struct wl_client *client, struct wl_resource *resource);
	static void set_minimized(struct wl_client *client, struct wl_resource *resource);

	static constexpr struct xdg_toplevel_interface iface = {
	    Toplevel::destroy,          Toplevel::set_parent,       Toplevel::set_title,
	    Toplevel::set_app_id,       Toplevel::show_window_menu, Toplevel::move,
	    Toplevel::resize,           Toplevel::set_max_size,     Toplevel::set_min_size,
	    Toplevel::set_maximized,    Toplevel::unset_maximized,  Toplevel::set_fullscreen,
	    Toplevel::unset_fullscreen, Toplevel::set_minimized,
	};

	struct Dimensions {
		int32_t width{0};
		int32_t height{0};
	} min_size_, max_size_;

	struct wl_resource *resource_;

	bool is_maximized_{false};
	bool is_active_{false};
	bool is_resizing_{false};

	struct wl_listener on_surface_destroy_;
	static void on_surface_destroy_cb(struct wl_listener *listener, void *user_data);

	void send_configuration(int32_t width, int32_t height);

	void apply_pending();

public:
	Toplevel(Surface *surface, struct wl_resource *resource);
	~Toplevel();

	void set_size(int32_t width, int32_t height) override;

	orbycomp::shell::Surface *
	surface() override
	{
		return surface_;
	}

	void set_activity(bool active) override;
	void set_maximized(bool maximized) override;

	void close() override;
};

class Popup : public orbycomp::shell::Popup
{
private:
	friend Surface;

	Surface *surface_;
	struct wl_resource *resource_;

	Surface *parent_;

	static void on_surface_destroy_cb(struct wl_listener *listener, void *user_data);

	static void on_resource_destroy(struct wl_resource *resource);

	static void destroy(struct wl_client *client, struct wl_resource *resource);
	static void grab(struct wl_client *client,
			 struct wl_resource *resource,
			 struct wl_resource *seat,
			 uint32_t serial);
	static void reposition(struct wl_client *client,
			       struct wl_resource *resource,
			       struct wl_resource *positioner,
			       uint32_t token);

	static constexpr struct xdg_popup_interface iface = {
	    Popup::destroy,
	    Popup::grab,
	    Popup::reposition,
	};

	struct wl_listener on_surface_destroy_;

public:
	Popup(Surface *surface, struct wl_resource *resource, Surface *parent);

	~Popup();

	Surface *
	surface() override
	{
		return surface_;
	}

	Surface *
	transient_for() override
	{
		return parent_;
	}

	void
	set_activity(bool) override
	{
	}
};

} // namespace xdg

} // namespace shell

} // namespace orbycomp
