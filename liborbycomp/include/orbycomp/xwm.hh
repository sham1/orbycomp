/*
 * xwm.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <algorithm>
#include <ev.h>
#include <memory>
#include <optional>
#include <orbycomp/base/shell.hh>
#include <string>
#include <unordered_map>
#include <variant>
#include <xcb/xcb.h>

#include <xcb/render.h>

#include <orbycomp/util/idle-event-queue.hh>

namespace orbycomp
{

class XWaylandCtx;

class XWindowManager
{
public:
	XWindowManager(XWaylandCtx *x_ctx);
	~XWindowManager();

private:
	XWaylandCtx *x_ctx_;

	struct XcbConnectionDestoryer {
		inline void
		operator()(xcb_connection_t *conn)
		{
			xcb_disconnect(conn);
		}
	};
	std::unique_ptr<xcb_connection_t, XcbConnectionDestoryer> x_conn_{nullptr};

	struct ev_io on_xcb_event_listener_ {
	};
	static void on_xcb_event(EV_P_ struct ev_io *w, int revents);

	xcb_screen_t *screen_{nullptr};
	xcb_window_t wm_window_{};

	orbycomp::shell::Shell *shell_{nullptr};

	void create_color_map();

	void create_wm_window();

	void get_resources();
	std::unordered_map<std::string, xcb_atom_t> atoms_{};

	void get_default_cursor();

	bool is_our_resource(uint32_t id);

	void handle_create_notify(struct xcb_create_notify_event_t *event);
	void handle_destroy_notify(struct xcb_destroy_notify_event_t *event);
	void handle_map_request(struct xcb_map_request_event_t *event);
	void handle_unmap_notify(struct xcb_unmap_notify_event_t *event);
	void handle_client_message(struct xcb_client_message_event_t *event);
	void handle_property_notify(struct xcb_property_notify_event_t *event);
	void handle_configure_request(struct xcb_configure_request_event_t *event);
	void handle_configure_notify(struct xcb_configure_notify_event_t *event);
	void handle_expose(struct xcb_expose_event_t *event);

	struct XWindow : public orbycomp::shell::Surface {
		struct Toplevel : public orbycomp::shell::Toplevel {
			XWindow *wnd_;

			Toplevel(XWindow *wnd) : wnd_{wnd} {}
			~Toplevel();

			orbycomp::shell::Surface *
			surface() override
			{
				return wnd_;
			}

			void set_size(int32_t width, int32_t height) override;
			void set_activity(bool activity) override;
			void set_maximized(bool maximize) override;

			bool active_{false};

			struct PendingConfig {
				bool maximized{false};
				int32_t width{-1};
				int32_t height{-1};
				bool active{false};

				PendingConfig() {}
			};
			std::optional<PendingConfig> pending_config_{std::nullopt};
			util::EventToken pending_config_send_token_{};
			void send_pending_config();

			void close() override;
		};

		struct Popup : public orbycomp::shell::Popup {
			XWindow *wnd_;
			XWindow *parent_;

			bool is_dialog_{false};

			Popup(XWindow *wnd, XWindow *parent) : wnd_{wnd}, parent_{parent} {}
			~Popup();

			orbycomp::shell::Surface *
			surface() override
			{
				return wnd_;
			}

			orbycomp::shell::Surface *transient_for() override;

			void set_activity(bool active) override;
		};

		XWindowManager *manager;

		xcb_window_t id{XCB_WINDOW_NONE};
		xcb_window_t frame_id{XCB_WINDOW_NONE};

		int x{0};
		int y{0};
		int width{-1};
		int height{-1};

		bool has_wm_delete_window_{false};

		bool maximized_vert_{false};
		bool maximized_horz_{false};

		bool
		is_maximized() const
		{
			return maximized_vert_ && maximized_horz_;
		}

		void read_properties();

		bool props_dirty{false};
		bool has_alpha{false};

		bool override_redirect{false};

		struct wl_resource *surface_resource_{nullptr};
		orbycomp::Surface *surface_{nullptr};
		struct wl_listener on_surface_destroy_ {
		};

		static void on_surface_destroy_cb(struct wl_listener *listener, void *data);

		std::variant<std::monostate, Toplevel, Popup> state_{std::monostate{}};

		XWindow(XWindowManager *mngr, xcb_window_t wnd) : manager{mngr}, id{wnd} {}

		~XWindow();

		orbycomp::Surface *
		surface() override
		{
			return surface_;
		}

		void associate_surface(orbycomp::Surface *surface);

		void map() override;
		void unmap() override;

		orbycomp::shell::Toplevel *toplevel() override;
		orbycomp::shell::Popup *popup() override;

		bool mapped_{false};

		XWindow *transient_for_{nullptr};

		void set_commit_allowance(bool allow);
		void set_net_wm_state();

		enum class ICCCMState : uint32_t {
			Withdrawn = 0,
			Normal = 1,
			Iconic = 3,
		};
		void send_wm_state(ICCCMState state);
	};

	std::unordered_map<xcb_window_t, XWindow> windows_{};
	std::unordered_map<uint32_t, XWindow *> unassociated_surfaces_{};

	sigc::connection on_surface_signal_conn_;
	void on_surface_created(Surface &surface);

	xcb_visualid_t visual_id_{};
	xcb_colormap_t colormap_{};
	xcb_render_pictforminfo_t format_rgba_{};

	void create_window(xcb_window_t window, int width, int height, int x, int y, bool override_redirect);

	void destroy_window(xcb_window_t window);

	void handle_surface_id(XWindow &wnd, struct xcb_client_message_event_t *event);
	void handle_net_wm_state(XWindow &wnd, struct xcb_client_message_event_t *event);

	auto
	find_window(xcb_window_t wid) -> decltype(windows_.end())
	{
		return std::find_if(windows_.begin(), windows_.end(), [wid](auto &p) {
			auto &[_, win] = p;
			return wid == win.id || wid == win.frame_id;
		});
	}

	bool is_popup(xcb_atom_t atom);
};

}; // namespace orbycomp
