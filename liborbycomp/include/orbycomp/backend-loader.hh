/*
 * backend-loader.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <dlfcn.h>
#include <memory>
#include <orbycomp/backend.hh>
#include <string>

namespace orbycomp
{

class Backend
{
	struct dynamic_handle_closer {
		void
		operator()(void *handle) const
		{
			dlclose(handle);
		}
	};

	std::unique_ptr<void, dynamic_handle_closer> handle_;

	std::unique_ptr<BackendBase> backend_;

	friend Backend load_backend(const std::string &name, Compositor *compositor);

	Backend(std::unique_ptr<BackendBase> &&backend, std::unique_ptr<void, dynamic_handle_closer> &&handle)
	    : handle_(std::move(handle)), backend_(std::move(backend))

	{
	}

public:
	Backend() = default;

	~Backend() = default;

	inline BackendBase &
	operator*() const
	{
		return *backend_.get();
	}

	inline BackendBase *
	operator->() const noexcept
	{
		return backend_.get();
	}

	Backend &
	operator=(Backend &&other)
	{
		this->backend_ = std::move(other.backend_);
		this->handle_ = std::move(other.handle_);

		return *this;
	}
};

Backend load_backend(const std::string &backend_name, Compositor *compositor);
Backend load_default_backend(Compositor *compositor);

} // namespace orbycomp
