/*
 * software-renderer.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/render/software-rendering.hh>
#include <orbycomp/renderer.hh>

#include <functional>

namespace orbycomp
{

namespace renderers
{

struct SoftwareRendererOptions : public RendererOptions {
};

struct SoftwareRendererOutputOptions : public RendererOutputOptions {
	using GetFramebufferFunc = std::function<orbycomp::rendering::software::BitmapSurface(void)>;

	GetFramebufferFunc get_fb;
};

}; // namespace renderers

}; // namespace orbycomp
