/*
 * renderer.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <orbycomp/render/software-rendering.hh>
#include <orbycomp/renderer.hh>
#include <orbycomp/renderers/software-renderer.hh>

extern "C" orbycomp::Renderer *renderer_init(orbycomp::RendererOptions &opts);

namespace orbycomp
{

namespace softrend
{

using Opts = orbycomp::renderers::SoftwareRendererOptions;
using OutputOpts = orbycomp::renderers::SoftwareRendererOutputOptions;

class Renderer;
class OutputInfo;

class Texture
{
	int32_t width_;
	int32_t height_;
	int32_t stride_;

	std::vector<uint8_t> data_;

public:
	Texture() = default;

	Texture(int32_t width, int32_t height, int32_t stride);

	~Texture() = default;

	uint8_t *
	data()
	{
		return data_.data();
	}

	Texture(const Texture &other) = delete;
	Texture &operator=(const Texture &other) = delete;

	Texture(Texture &&other)
	    : width_{std::move(other.width_)}, height_{std::move(other.height_)},
	      stride_{std::move(other.stride_)}, data_{std::move(other.data_)}
	{
	}

	Texture &
	operator=(Texture &&other)
	{
		width_ = std::move(other.width_);
		height_ = std::move(other.height_);
		stride_ = std::move(other.stride_);
		data_ = std::move(other.data_);

		return *this;
	}

	rendering::software::BitmapSurface
	get_bitmap_surface()
	{
		return rendering::software::BitmapSurface{data_.data(), size_t(stride_), width_, height_};
	}
};

class SoftwareRenderable
{
public:
	virtual void calculate_input_region(Region &region) = 0;
	virtual void calculate_opaque_region(Region &region) = 0;

	virtual const Region &transparent_region() = 0;

	virtual Texture &get_texture() = 0;

	virtual ~SoftwareRenderable() {}
};

class SurfaceRenderable : public orbycomp::SurfaceRenderable, public SoftwareRenderable
{
	Renderer *renderer_;
	Texture texture_;

	Region transparent_region_;

	bool has_alpha_{true};

public:
	SurfaceRenderable(Renderer *renderer, Surface *surface);
	~SurfaceRenderable();

	void update() override;
	void upload_texture(bool attach) override;

	void render_finish(uint32_t msec_delta) override;

	void calculate_input_region(Region &region) override;
	void calculate_opaque_region(Region &region) override;

	const Region &transparent_region() override;

	Texture &
	get_texture() override
	{
		return texture_;
	}
};

class BitmapRenderable : public orbycomp::BitmapRenderable, public SoftwareRenderable
{
	Renderer *renderer_;
	Texture texture_;

	Region transparent_region_;

public:
	BitmapRenderable(
	    Renderer *renderer, int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha);
	~BitmapRenderable();

	void update() override;
	void upload_texture(bool attach) override;

	void render_finish(uint32_t msec_delta) override;

	void calculate_input_region(Region &region) override;
	void calculate_opaque_region(Region &region) override;

	const Region &transparent_region() override;

	Texture &
	get_texture() override
	{
		return texture_;
	}
};

class Renderer : public orbycomp::Renderer
{
	friend orbycomp::Renderer * ::renderer_init(orbycomp::RendererOptions &opts);

	friend OutputInfo;
	friend SurfaceRenderable;
	friend BitmapRenderable;

	Compositor *compositor_;

	Renderer(Opts &opts);

public:
	~Renderer();

	std::shared_ptr<orbycomp::SurfaceRenderable> get_surface_renderable(Surface *surface) override;

	std::shared_ptr<orbycomp::BitmapRenderable> get_bitmap_renderable(
	    int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha = true) override;

	void set_cursor(Pointer *pointer, Renderable *renderable, int32_t hotx, int32_t hoty) override;

	void hide_cursor() override;

	void setup_output(Output *output, RendererOutputOptions &opts) override;

	void render_output(Output *output) override;

	std::unordered_map<Output *, std::unique_ptr<OutputInfo>> outputs_;
};

class OutputInfo
{
	friend Renderer;

	Renderer *renderer_;
	Output *output_;

	using GetFBFunc = OutputOpts::GetFramebufferFunc;
	GetFBFunc get_fb_;

public:
	OutputInfo(Renderer *renderer, Output *output, GetFBFunc get_fb);
	~OutputInfo();

	void render();
};

} // namespace softrend

} // namespace orbycomp
