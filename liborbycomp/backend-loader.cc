/*
 * backend-loader.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp-conf.h>
#include <orbycomp/backend-loader.hh>
#include <orbycomp/base/compositor.hh>
#include <sstream>
#include <stdexcept>

namespace orbycomp
{

Backend
load_backend(const std::string &backend_name, Compositor *compositor)
{
	using BackendInit = BackendBase *(*) (Compositor *);

	std::unique_ptr<void, Backend::dynamic_handle_closer> backend_handle(
	    dlopen(backend_name.c_str(), RTLD_LAZY));

	if (backend_handle == nullptr) {
		throw std::runtime_error(dlerror());
	}

	// Clear any existing errors.
	dlerror();

	BackendInit init = reinterpret_cast<BackendInit>(dlsym(backend_handle.get(), "backend_init"));

	{
		const char *error_tmp = nullptr;
		if ((error_tmp = dlerror()) != nullptr) {
			throw std::runtime_error(error_tmp);
		}
	}

	std::unique_ptr<BackendBase> backend(init(compositor));

	return {std::move(backend), std::move(backend_handle)};
}

Backend
load_default_backend(Compositor *compositor)
{
#ifdef ENABLED_WAYLAND_BACKEND
	try {
		return load_backend("liborbycomp-wayland-backend.so", compositor);
	} catch (const std::runtime_error &e) {
		std::fprintf(stderr, "Couldn't load Wayland backend: %s\n", e.what());
		std::fprintf(stderr, "Trying next backend...\n");
	}
#endif

#ifdef ENABLED_DRM_BACKEND
	try {
		return load_backend("liborbycomp-drm-backend.so", compositor);
	} catch (const std::runtime_error &e) {
		std::fprintf(stderr, "Couldn't load DRM backend: %s\n", e.what());
		std::fprintf(stderr, "Trying next backend...\n");
	}
#endif

	throw std::runtime_error("Couldn't find suitable compositor backend");
}

} // namespace orbycomp
