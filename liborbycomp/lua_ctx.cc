/*
 * lua_ctx.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <filesystem>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/lua_ctx.hh>

namespace fs = std::filesystem;

namespace orbycomp
{

LuaCtx::LuaCtx(Compositor *compositor) : compositor_{compositor} {}

void
LuaCtx::load_user_init()
{
	// We have to find out where the user's `init.lua` is.
	// The location is `$XDG_CONFIG_HOME/orbycomp/init.lua`.
	// First, we check whether XDG_CONFIG_HOME is set. If so,
	// use that for constructing the path.
	//
	// If not set, default to `$HOME/.config/orbycomp/init.lua`.
	//
	// If not found from either location, use a default
	// system-wide configuration.

	std::string user_config_path{};
	const char *xdg_config_home = getenv("XDG_CONFIG_HOME");
	if (xdg_config_home != nullptr) {
		user_config_path.append(xdg_config_home);
	} else {
		const char *home = getenv("HOME");
		user_config_path.append(home).append("/.config");
	}

	user_config_path.append("/orbycomp/init.lua");

	std::fprintf(stderr, "Looking for user configuration file: %s\n", user_config_path.c_str());

	bool loaded = false;
	if (fs::exists(user_config_path)) {
		std::fprintf(stderr, "Configuration file found! Loading...\n");

		int ret = luaL_loadfile(ctx_.L(), user_config_path.c_str());
		if (ret != 0) {
			std::fprintf(stderr, "Error loading configuration file: %s\n",
				     ctx_.get<std::string>(-1).c_str());
			ctx_.pop(1);
		} else {
			ret = ctx_.pcall(0, LUA_MULTRET);
			if (ret != 0) {
				std::fprintf(stderr,
					     "Error while executing "
					     "configuration file:\n"
					     "%s\n",
					     ctx_.get<std::string>(-1).c_str());
				ctx_.pop(1);
			}
			loaded = ret == 0;
		}
	} else {
		std::fprintf(stderr, "Could not find user configuration file.\n");
	}

	if (!loaded) {
		std::fprintf(stderr, "Using default configuration.\n");
		// TODO: Implement.
		loaded = true;
	}
}

LuaCtx::~LuaCtx() {}

} // namespace orbycomp
