/*
 * gles-renderer.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "backend.hh"
#include "renderer.hh"

namespace orbycomp
{

namespace drm
{

class GLESRenderer : public Renderer
{
	class GBMSurface
	{
		struct gbm_surface *surface_;

	public:
		inline struct gbm_surface *
		get()
		{
			return surface_;
		}

		GBMSurface(GBMDevice &dev, uint32_t width, uint32_t height, uint32_t format, uint32_t flags)
		    : surface_{gbm_surface_create(dev.get(), width, height, format, flags)}
		{
			if (!surface_) {
				throw std::runtime_error("Could not create GBM surface");
			}
		}

		~GBMSurface()
		{
			if (surface_) {
				gbm_surface_destroy(surface_);
				surface_ = nullptr;
			}
		}

		GBMSurface(const GBMSurface &) = delete;
		GBMSurface &operator=(const GBMSurface &) = delete;

		GBMSurface(GBMSurface &&other) : surface_{std::move(other.surface_)}
		{
			other.surface_ = nullptr;
		}

		GBMSurface &
		operator=(GBMSurface &&other)
		{
			surface_ = std::move(other.surface_);
			other.surface_ = nullptr;

			return *this;
		}
	};

	class OutputInfo
	{
		friend GLESRenderer;

		GLESRenderer *renderer_;
		Output *output_;

		GBMSurface surface_;

		struct gbm_bo *bo_{nullptr};

	public:
		OutputInfo(GLESRenderer *renderer, Output *output, GBMSurface &&surface);
		~OutputInfo();

		SurfaceFB *render();
	};

	static RendererHandle load_renderer(Backend *backend, struct gbm_device *dev);

	GLESRenderer(Backend *backend, GBMDevice &&dev);

	GBMDevice gbm_dev_;

	std::unordered_map<Output *, std::unique_ptr<OutputInfo>> outputs_;

public:
	static std::unique_ptr<GLESRenderer> setup(Backend *backend);

	void setup_output(Output *output) override;

	SurfaceFB *render_output(Output *output) override;
};

}; // namespace drm

}; // namespace orbycomp
