/*
 * gles2-renderer.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "backend.hh"
#include "renderer.hh"

#include <orbycomp/render/shader.hh>

#include <gbm.h>
#include <memory>
#include <orbycomp/base/surface.hh>
#include <unordered_set>

namespace
{
typedef EGLBoolean(EGLAPIENTRYP PFNEGLBINDWAYLANDDISPLAYWL)(EGLDisplay dpy, struct wl_display *display);
typedef EGLBoolean(EGLAPIENTRYP PFNEGLUNBINDWAYLANDDISPLAYWL)(EGLDisplay dpy, struct wl_display *display);
typedef EGLBoolean(EGLAPIENTRYP PFNEGLQUERYWAYLANDBUFFERWL)(EGLDisplay dpy,
							    struct wl_resource *buffer,
							    EGLint attribute,
							    EGLint *value);
}; // namespace

#ifndef EGL_WAYLAND_BUFFER_WL
#define EGL_WAYLAND_BUFFER_WL 0x31D5
#define EGL_WAYLAND_PLANE_WL 0x31D6

#define EGL_TEXTURE_Y_U_V_WL 0x31D7
#define EGL_TEXTURE_Y_UV_WL 0x31D8
#define EGL_TEXTURE_Y_XUXV_WL 0x31D9
#define EGL_TEXTURE_EXTERNAL_WL 0x31DA
#endif

namespace orbycomp
{

namespace drm
{

class GLES2Renderer;
class GLES2SurfaceRenderable;
class GLES2BitmapRenderable;

class GLES2RendererOutputInfo
{
	friend GLES2Renderer;
	friend GLES2SurfaceRenderable;
	friend GLES2BitmapRenderable;

	GLES2Renderer *renderer_;
	Output *output_;

	using GBMSurface = std::unique_ptr<struct gbm_surface, decltype(&gbm_surface_destroy)>;

	GBMSurface surface_;
	struct gbm_bo *bo_{nullptr};
	struct gbm_bo *next_bo_{nullptr};

	class SurfaceFB
	{
		friend GLES2RendererOutputInfo;

		GLES2RendererOutputInfo *info_;
		struct gbm_bo *bo_;
		uint32_t fb_{0};

		uint32_t create_fb_from_bo(struct gbm_bo *bo);

	public:
		SurfaceFB(GLES2RendererOutputInfo *info, struct gbm_bo *bo);
		~SurfaceFB();

		static void on_fb_destroy_cb(struct gbm_bo *bo, void *data);

		static SurfaceFB *get_from_bo(GLES2RendererOutputInfo *info, struct gbm_bo *bo);
	};

	EGLConfig egl_config_;
	EGLContext egl_context_;
	EGLSurface egl_surface_;

	EGLConfig get_config();
	EGLSurface create_egl_surface();
	EGLContext get_context();

	opengl::LinkedShaderProgram renderable_shader_;

public:
	GLES2RendererOutputInfo(GLES2Renderer *renderer, Output *output, GBMSurface &&surface);
	~GLES2RendererOutputInfo();

	void render_output();

	void render();

	void set_crtc();

	bool needs_repaint{true};
	bool is_drawing{false};
};

class GLES2Renderer : public Renderer
{
	friend GLES2SurfaceRenderable;
	friend GLES2BitmapRenderable;
	friend GLES2RendererOutputInfo;

	class GBMDevice
	{
		struct gbm_device *device_;

	public:
		inline struct gbm_device *
		get()
		{
			return device_;
		}

		GBMDevice(int fd) : device_{gbm_create_device(fd)}
		{
			if (!device_) {
				throw std::runtime_error("Could not setup GBM");
			}
		}

		~GBMDevice()
		{
			if (device_) {
				gbm_device_destroy(device_);
				device_ = nullptr;
			}
		}
	} device_;

	class egl_display
	{
		GLES2Renderer *renderer_;
		EGLDisplay dpy;
		std::vector<EGLConfig> configs_{};

	public:
		inline EGLDisplay
		get()
		{
			return dpy;
		}

		inline const std::vector<EGLConfig> &
		configs() const
		{
			return configs_;
		}

		egl_display(GLES2Renderer *renderer);

		~egl_display();

		std::unordered_set<std::string> client_exts_;
		std::unordered_set<std::string> display_exts_;

		PFNEGLBINDWAYLANDDISPLAYWL bind_wayland_display_wl;
		PFNEGLUNBINDWAYLANDDISPLAYWL unbind_wayland_display_wl;
		PFNEGLQUERYWAYLANDBUFFERWL query_wayland_buffer_wl;
	} display_;

	drmEventContext event_ctx_{};

	std::unordered_map<std::shared_ptr<Output>, GLES2RendererOutputInfo> output_info_{};

	static void on_page_flip(
	    int fd, unsigned int sequence, unsigned int tv_sec, unsigned int tv_usec, void *user_data);

	struct CursorData {
		Pointer *pointer;
		int32_t hotx;
		int32_t hoty;
		sigc::connection pointer_move_connection;
	};
	std::optional<CursorData> cursor_{};
	std::unique_ptr<struct gbm_bo, decltype(&gbm_bo_destroy)> cursor_bo_;

	void activate_cursor();
	void set_cursor_position(double new_x, double new_y);
	bool render_cursor_to_bo(Renderable *renderable);

public:
	GLES2Renderer(Backend *backend);
	~GLES2Renderer() {}

	std::shared_ptr<SurfaceRenderable> get_surface_renderable(Surface *surface) override;

	std::shared_ptr<BitmapRenderable> get_bitmap_renderable(
	    int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha = true) override;

	void
	setup_output(orbycomp::Output *output, RendererOutputOptions &opts) override
	{
	}

	void setup_display(std::shared_ptr<Output> output) override;

	void queue_render_full(std::shared_ptr<Output> output) override;

	drmEventContextPtr drm_event_context() override;

	void set_cursor(Pointer *pointer, Renderable *renderable, int32_t hotx, int32_t hoty) override;

	void hide_cursor() override;

	bool is_buffer_egl(struct wl_resource *buffer);
};

class GLES2Renderable
{
public:
	bool is_cursor_{false};
	bool is_opaque_{true};

	virtual ~GLES2Renderable() {}
	virtual void calculate_input_region(Region &input_region) = 0;
};

class GLES2SurfaceRenderable : public SurfaceRenderable, public GLES2Renderable
{
	GLuint texture_{0};
	GLES2Renderer *renderer_;

public:
	GLES2SurfaceRenderable(Surface *surface, GLES2Renderer *renderer);
	~GLES2SurfaceRenderable();

	void render_finish(uint32_t msec_delta) override;

	void update() override;

	void bind_texture_to_loc(GLint loc);

	bool is_rendering_{false};

	void calculate_input_region(Region &input_region) override;
};

class GLES2BitmapRenderable : public BitmapRenderable, public GLES2Renderable
{
	friend GLES2Renderer;

	GLuint texture_{0};
	GLES2Renderer *renderer_;

public:
	GLES2BitmapRenderable(int32_t x, int32_t y, int32_t width, int32_t height, bool is_alpha);
	~GLES2BitmapRenderable();

	void render_finish(uint32_t msec_delta) override;

	void update() override;

	void bind_texture_to_loc(GLint loc);

	void calculate_input_region(Region &input_region) override;
};

} // namespace drm

} // namespace orbycomp
