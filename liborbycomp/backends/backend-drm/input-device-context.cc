/*
 * input-device-context.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "input-device-context.hh"
#include "backend.hh"
#include "input-device.hh"
#include <cerrno>
#include <orbycomp/base/compositor.hh>
#include <system_error>

namespace orbycomp
{

namespace drm
{

struct libinput_interface control_iface {
	InputDeviceCtx::open_restricted, InputDeviceCtx::close_restricted,
};

InputDeviceCtx::InputDeviceCtx(Backend *backend)
    : backend_{backend}, ctx_{libinput_udev_create_context(&control_iface, this, backend_->udev_ctx.get())}
{
	if (!ctx_) {
		throw std::bad_alloc();
	}

	libinput_udev_assign_seat(ctx_.get(), backend_->seat_name().c_str());
	libinput_resume(ctx_.get());

	int libinput_fd = libinput_get_fd(ctx_.get());

	ev_io_init(&libinput_dispatch, InputDeviceCtx::on_libinput_event_cb, libinput_fd, EV_READ);
	libinput_dispatch.data = this;
	ev_io_start(backend_->compositor()->loop(), &libinput_dispatch);

	backend_->compositor()->activity_change_signal.connect(
	    [this](bool active) { this->on_compositor_activity_change(active); });
}

InputDeviceCtx::~InputDeviceCtx()
{
	if (ev_is_active(&libinput_dispatch)) {
		ev_io_stop(backend_->compositor()->loop(), &libinput_dispatch);
	}
}

int
InputDeviceCtx::open_restricted(const char *path, int flags, void *user_data)
{
	InputDeviceCtx *self = static_cast<InputDeviceCtx *>(user_data);
	try {
		return self->backend_->session_manager->open(path, flags);
	} catch (const std::bad_alloc &) {
		return -ENOMEM;
	} catch (const std::system_error &e) {
		return -e.code().value();
	}
}

void
InputDeviceCtx::close_restricted(int fd, void *user_data)
{
	InputDeviceCtx *self = static_cast<InputDeviceCtx *>(user_data);
	self->backend_->session_manager->close(fd);
}

void
InputDeviceCtx::on_libinput_event_cb(EV_P_ struct ev_io *w, int revents)
{
	InputDeviceCtx *self = static_cast<InputDeviceCtx *>(w->data);
	self->dispatch_events();
}

void
InputDeviceCtx::dispatch_events()
{
	::libinput_dispatch(ctx_.get());

	struct libinput_event *event_;
	while ((event_ = libinput_get_event(ctx_.get()))) {
		std::unique_ptr<struct libinput_event, decltype(&libinput_event_destroy)> event{
		    event_, libinput_event_destroy};

		struct libinput_device *device = libinput_event_get_device(event.get());
		enum libinput_event_type type = libinput_event_get_type(event.get());

		if (type == LIBINPUT_EVENT_DEVICE_ADDED) {
			InputDevice::create_device(backend_, device);
			continue;
		}

		InputDevice *input_device = static_cast<InputDevice *>(libinput_device_get_user_data(device));
		if (type == LIBINPUT_EVENT_DEVICE_REMOVED) {
			delete input_device;
			continue;
		}

		if (input_device) {
			input_device->dispatch(event.get());
		}
	}
}

void
InputDeviceCtx::on_compositor_activity_change(bool active)
{
	if (active) {
		ev_io_set(&libinput_dispatch, libinput_get_fd(ctx_.get()), EV_READ);
		libinput_dispatch.data = this;
		ev_io_start(backend_->compositor()->loop(), &libinput_dispatch);
		libinput_resume(ctx_.get());
	} else {
		ev_io_stop(backend_->compositor()->loop(), &libinput_dispatch);
		libinput_suspend(ctx_.get());
	}
	dispatch_events();
}

} // namespace drm

} // namespace orbycomp
