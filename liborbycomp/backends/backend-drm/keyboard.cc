/*
 * keyboard.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "keyboard.hh"
#include "backend.hh"
#include <cstdio>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/keyboard.hh>
#include <orbycomp/base/seat.hh>

namespace orbycomp
{

namespace drm
{

void
Keyboard::dispatch(struct libinput_event *event)
{
	enum libinput_event_type type = libinput_event_get_type(event);

	switch (type) {
	case LIBINPUT_EVENT_KEYBOARD_KEY:
		break;
	default:
		return;
	}

	struct libinput_event_keyboard *key_event = libinput_event_get_keyboard_event(event);

	orbycomp::Keyboard::KeyState state;
	enum libinput_key_state native_state = libinput_event_keyboard_get_key_state(key_event);
	if (native_state == LIBINPUT_KEY_STATE_PRESSED) {
		state = orbycomp::Keyboard::KeyState::Press;
	}
	if (native_state == LIBINPUT_KEY_STATE_RELEASED) {
		state = orbycomp::Keyboard::KeyState::Release;
	}

	uint32_t tv_msec = libinput_event_keyboard_get_time(key_event);

	if (seat_) {
		if (seat_->keyboard()) {
			uint32_t key = libinput_event_keyboard_get_key(key_event);
			seat_->keyboard()->feed_native_keypress(key, state, tv_msec);
		}
	}
}

} // namespace drm

} // namespace orbycomp
