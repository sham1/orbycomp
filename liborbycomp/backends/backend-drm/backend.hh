/*
 * backend.hh
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "input-device-context.hh"
#include "renderer.hh"
#include "session-manager.hh"
#include <libudev.h>
#include <memory>
#include <orbycomp/backend.hh>
#include <orbycomp/base/keybinds.hh>
#include <orbycomp/base/output.hh>
#include <orbycomp/base/renderable.hh>
#include <string>
#include <vector>
#include <xf86drm.h>
#include <xf86drmMode.h>

namespace orbycomp
{

namespace drm
{

using ManagedDrmModeConnector = std::unique_ptr<drmModeConnector, decltype(&drmModeFreeConnector)>;

extern "C" BackendBase *backend_init(Compositor *);

class Output;

class Backend : public BackendBase
{
	friend BackendBase *backend_init(Compositor *);
	friend Output;
	friend Renderer;

	Backend(Compositor *compositor);

	std::string seat_name_;

	void switch_vty_cb(Modifier mods, xkb_keysym_t sym);
	std::vector<Keybind> vty_switch_binds{};

	static void on_drm_event_cb(EV_P_ ev_io *w, int revents);

	void modeset_outputs();

	sigc::connection on_compositor_active_connection_;

	drmEventContext event_ctx_;
	static void on_page_flip(
	    int fd, unsigned int sequence, unsigned int tv_sec, unsigned int tv_usec, void *user_data);

public:
	~Backend();

	inline Compositor *
	compositor()
	{
		return this->compositor_;
	}

	inline const std::string &
	seat_name() const
	{
		return seat_name_;
	}

	std::unique_ptr<SessionManager> session_manager;
	std::vector<Output *> outputs_{};

	struct DrmDevice {
		int fd_{-1};
		unsigned int major_{0};
		unsigned int minor_{0};
		bool active_{false};
		Backend *backend_{nullptr};
		drm_magic_t magic;

		std::unique_ptr<drmModeRes, decltype(&drmModeFreeResources)> res_{nullptr,
										  drmModeFreeResources};

		void create_output(ManagedDrmModeConnector &&connector);

	public:
		DrmDevice() = default;
		DrmDevice(Backend *backend);

		~DrmDevice();

		inline int
		fd() const
		{
			return fd_;
		}

		inline unsigned int
		major() const
		{
			return major_;
		}

		inline unsigned int
		minor() const
		{
			return minor_;
		}
	} drm_dev;

	struct ev_io drm_event_handler_ {
	};

	class UdevContext
	{
		struct udev_context_closer {
			void
			operator()(struct udev *handle) const
			{
				udev_unref(handle);
			}
		};
		std::unique_ptr<struct udev, udev_context_closer> udev_;

	public:
		UdevContext() : udev_{udev_new()} {}
		~UdevContext() {}

		inline struct udev *
		get()
		{
			return udev_.get();
		}
	} udev_ctx{};

	InputDeviceCtx input_ctx;

	std::unique_ptr<Renderer> renderer_;

	void on_device_pause(unsigned int major, unsigned int minor);
	void on_device_resume(unsigned int major, unsigned int minor, int new_fd);

	orbycomp::Renderer *get_renderer() override;

	void repaint_output(orbycomp::Output *output) override;
};

using ManagedDrmCrtcInfo = std::unique_ptr<drmModeCrtc, decltype(&drmModeFreeCrtc)>;
using ManagedDrmEncoder = std::unique_ptr<drmModeEncoder, decltype(&drmModeFreeEncoder)>;

class Output : public orbycomp::Output
{
public:
	Output(Backend *backend,
	       ManagedDrmModeConnector &&connector,
	       int32_t x,
	       int32_t y,
	       std::vector<Mode> &&modes,
	       int32_t scale,
	       enum wl_output_transform transform,
	       const std::string &make,
	       const std::string &model,
	       int current_mode);

	~Output();

	friend Backend;

	Backend *backend_;
	ManagedDrmModeConnector connector_;
	int current_mode_;
	ManagedDrmEncoder encoder_;
	ManagedDrmCrtcInfo original_crtc_info_;

	std::shared_ptr<BitmapRenderable> background_renderable_;

private:
	ManagedDrmCrtcInfo get_original_crtc_info();
	ManagedDrmEncoder find_encoder();

	void modeset(uint32_t fb);
	void pageflip(uint32_t fb);
};

const inline enum wl_output_subpixel
drm_subpixel_to_wl_subpixel(drmModeSubPixel sub)
{
	switch (sub) {
	case DRM_MODE_SUBPIXEL_NONE:
		return WL_OUTPUT_SUBPIXEL_NONE;
	case DRM_MODE_SUBPIXEL_HORIZONTAL_RGB:
		return WL_OUTPUT_SUBPIXEL_HORIZONTAL_RGB;
	case DRM_MODE_SUBPIXEL_HORIZONTAL_BGR:
		return WL_OUTPUT_SUBPIXEL_HORIZONTAL_BGR;
	case DRM_MODE_SUBPIXEL_VERTICAL_RGB:
		return WL_OUTPUT_SUBPIXEL_VERTICAL_RGB;
	case DRM_MODE_SUBPIXEL_VERTICAL_BGR:
		return WL_OUTPUT_SUBPIXEL_VERTICAL_BGR;
	default:
		return WL_OUTPUT_SUBPIXEL_UNKNOWN;
	}
}

} // namespace drm

} // namespace orbycomp
