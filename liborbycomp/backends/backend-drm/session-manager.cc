#include "session-manager.hh"
#include "backend.hh"
#include "session-manager-logind.hh"
#include <cstdio>
#include <orbycomp-conf.h>
#include <stdexcept>

namespace orbycomp
{

namespace drm
{

std::unique_ptr<SessionManager>
SessionManager::load(Backend *backend)
{
#ifdef ENABLED_DBUS

#ifdef ENABLED_LOGIND
	try {
		return std::make_unique<SessionManagerLogind>(backend);
	} catch (const std::exception &e) {
		std::fprintf(stderr, "Couldn't load logind session manager: %s\n", e.what());
		std::fprintf(stderr, "Trying next session manager...\n");
	}
#endif

#endif

	throw std::runtime_error("No suitable session backend found");
}

} // namespace drm

} // namespace orbycomp
