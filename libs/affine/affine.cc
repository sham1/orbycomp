/*
 * affine.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <algorithm>
#include <cmath>
#include <orbycomp/affine.hh>
#include <stdexcept>

static inline const bool
floats_equal(float a, float b)
{
	constexpr float epsilon = 1e-5;
	return std::abs(a - b) < epsilon;
}

namespace affine
{

Mat4 &
Mat4::operator*=(const Mat4 &rhs)
{
	*this = operator*(*this, rhs);
	return *this;
}

Mat4
operator*(float lhs, const Mat4 &rhs)
{
	using Basis = Mat4::Basis;
	Mat4 ret(rhs);
	ret[Basis::I] *= lhs;
	ret[Basis::J] *= lhs;
	ret[Basis::K] *= lhs;
	ret[Basis::W] *= lhs;
	return ret;
}

Mat4
operator*(const Mat4 &lhs, const Mat4 &rhs)
{
	using Basis = Mat4::Basis;
	Vec4 i = lhs * rhs[Basis::I];
	Vec4 j = lhs * rhs[Basis::J];
	Vec4 k = lhs * rhs[Basis::K];
	Vec4 w = lhs * rhs[Basis::W];

	return Mat4{i, j, k, w};
}

Vec4
operator*(const Mat4 &lhs, const Vec4 &rhs)
{
	using Basis = Mat4::Basis;
	using Axis = Vec4::Axis;
	Vec4 transformed_basis[4]{{}};
	transformed_basis[0] = rhs[Axis::X] * lhs[Basis::I];
	transformed_basis[1] = rhs[Axis::Y] * lhs[Basis::J];
	transformed_basis[2] = rhs[Axis::Z] * lhs[Basis::K];
	transformed_basis[3] = rhs[Axis::W] * lhs[Basis::W];

	Vec4 ret = transformed_basis[0] + transformed_basis[1] + transformed_basis[2] + transformed_basis[3];
	return ret;
}

bool
Vec4::operator==(const Vec4 &rhs) const
{
	return floats_equal(data_[0], rhs.data_[0]) && floats_equal(data_[1], rhs.data_[1]) &&
	       floats_equal(data_[2], rhs.data_[2]) && floats_equal(data_[3], rhs.data_[3]);
}

bool
Vec4::operator!=(const Vec4 &rhs) const
{
	return !this->operator==(rhs);
}

const static inline std::size_t
get_axis_idx(Vec4::Axis axis)
{
	using Axis = Vec4::Axis;

	switch (axis) {
	case Axis::X:
		return 0;
	case Axis::Y:
		return 1;
	case Axis::Z:
		return 2;
	case Axis::W:
		return 3;
	default:
		throw std::out_of_range("Vec4 can only be indexed by X, Y, Z or W");
	}
}

float
Vec4::operator[](Axis axis) const
{
	return data_[get_axis_idx(axis)];
}

float &
Vec4::operator[](Axis axis)
{
	return data_[get_axis_idx(axis)];
}

Vec4
Mat4::operator[](Basis basis) const
{
	switch (basis) {
	case Basis::I:
		return i_;
	case Basis::J:
		return j_;
	case Basis::K:
		return k_;
	case Basis::W:
		return w_;
	default:
		throw std::out_of_range("Matricies only have basis I, J, K and W");
	}
}

Vec4 &
Mat4::operator[](Basis basis)
{
	switch (basis) {
	case Basis::I:
		return i_;
	case Basis::J:
		return j_;
	case Basis::K:
		return k_;
	case Basis::W:
		return w_;
	default:
		throw std::out_of_range("Matricies only have basis I, J, K and W");
	}
}

Mat4::operator std::array<float, 16>() const
{
	const std::array<float, 4> &i = this->operator[](Basis::I);
	const std::array<float, 4> &j = this->operator[](Basis::J);
	const std::array<float, 4> &k = this->operator[](Basis::K);
	const std::array<float, 4> &w = this->operator[](Basis::W);

	std::array<float, 16> ret{};

	auto out = ret.begin();
	out = std::copy(i.begin(), i.end(), out);
	out = std::copy(j.begin(), j.end(), out);
	out = std::copy(k.begin(), k.end(), out);
	out = std::copy(w.begin(), w.end(), out);

	return ret;
}

bool
Mat4::operator==(const Mat4 &rhs) const
{
	return i_ == rhs.i_ && j_ == rhs.j_ && k_ == rhs.k_ && w_ == rhs.w_;
}

bool
Mat4::operator!=(const Mat4 &rhs) const
{
	return !this->operator==(rhs);
}

Vec4
operator*(float lhs, const Vec4 &rhs)
{
	using Axis = Vec4::Axis;
	Vec4 ret{};
	ret[Axis::X] = lhs * rhs[Axis::X];
	ret[Axis::Y] = lhs * rhs[Axis::Y];
	ret[Axis::Z] = lhs * rhs[Axis::Z];
	ret[Axis::W] = lhs * rhs[Axis::W];
	return ret;
}

Vec4
operator+(const Vec4 &lhs, const Vec4 &rhs)
{
	using Axis = Vec4::Axis;
	Vec4 ret{};
	ret[Axis::X] = lhs[Axis::X] + rhs[Axis::X];
	ret[Axis::Y] = lhs[Axis::Y] + rhs[Axis::Y];
	ret[Axis::Z] = lhs[Axis::Z] + rhs[Axis::Z];
	ret[Axis::W] = lhs[Axis::W] + rhs[Axis::W];
	return ret;
}

Vec4
Vec4::operator*(float rhs) const
{
	return rhs * *this;
}

Vec4 &
Vec4::operator*=(float rhs)
{
	*this = this->operator*(rhs);
	return *this;
}

Vec4
Vec4::operator/(float rhs) const
{
	return (1.0f / rhs) * *this;
}

Vec4 &
Vec4::operator/=(float rhs)
{
	*this = this->operator/(rhs);
	return *this;
}

float
Vec3::dot(const Vec3 &other) const
{
	return x * other.x + y * other.y + z * other.z;
}

Vec3
Vec3::cross(const Vec3 &other) const
{
	Vec3 cross;
	cross.x = this->y * other.z - this->z * other.y;
	cross.y = this->z * other.x - this->x * other.z;
	cross.z = this->x * other.y - this->y * other.x;

	return cross;
}

float
Vec3::magnitude() const
{
	return std::sqrt(this->dot(*this));
}

Vec3
Vec3::normalized() const
{
	return this->operator/(this->magnitude());
}

Vec3 &
Vec3::normalize()
{
	*this = this->normalized();
	return *this;
}

Vec3
Vec3::operator/(float rhs) const
{
	Vec3 ret(*this);
	ret.x /= rhs;
	ret.y /= rhs;
	ret.z /= rhs;

	return ret;
}

Vec3 &
Vec3::operator/=(float rhs)
{
	return *this;
}

Vec3
Vec3::operator+(const Vec3 &rhs) const
{
	Vec3 ret;
	ret.x = this->x + rhs.x;
	ret.y = this->y + rhs.y;
	ret.z = this->z + rhs.z;

	return ret;
}

Vec3 &
Vec3::operator+=(const Vec3 &rhs)
{
	*this = this->operator+(rhs);
	return *this;
}

Vec3
Vec3::operator-() const
{
	Vec3 ret;
	ret.x = -(this->x);
	ret.y = -(this->y);
	ret.z = -(this->z);

	return ret;
}

Vec3
Vec3::operator-(const Vec3 &rhs) const
{
	return this->operator+(-rhs);
}

Vec3 &
Vec3::operator-=(const Vec3 &rhs)
{
	*this = this->operator-(rhs);
	return *this;
}

Vec3
operator*(float lhs, const Vec3 &rhs)
{
	Vec3 ret;
	ret.x = lhs * rhs.x;
	ret.y = lhs * rhs.y;
	ret.z = lhs * rhs.z;

	return ret;
}

Vec3
operator*(const Mat4 &lhs, const Vec3 &rhs)
{
	Vec4 tmp = rhs;

	tmp = lhs * tmp;

	return {tmp[Vec4::Axis::X], tmp[Vec4::Axis::Y], tmp[Vec4::Axis::Z]};
}

Point3
operator*(const Mat4 &lhs, const Point3 &rhs)
{
	Vec4 tmp = rhs;

	tmp = lhs * tmp;

	return {tmp[Vec4::Axis::X], tmp[Vec4::Axis::Y], tmp[Vec4::Axis::Z]};
}

Point3
operator+(const Vec3 &lhs, const Point3 &rhs)
{
	Point3 ret;
	ret.x = rhs.x + lhs.x;
	ret.y = rhs.y + lhs.y;
	ret.z = rhs.z + lhs.z;
	return ret;
}

Vec3
operator-(const Point3 &lhs, const Point3 &rhs)
{
	Vec3 ret;
	ret.x = lhs.x - rhs.x;
	ret.y = lhs.y - rhs.y;
	ret.z = lhs.z - rhs.z;
	return ret;
}

bool
Vec3::operator==(const Vec3 &rhs) const
{
	return floats_equal(x, rhs.x) && floats_equal(y, rhs.y) && floats_equal(z, rhs.z);
}

bool
Vec3::operator!=(const Vec3 &rhs) const
{
	return !this->operator==(rhs);
}

bool
Point3::operator==(const Point3 &rhs) const
{
	return floats_equal(x, rhs.x) && floats_equal(y, rhs.y) && floats_equal(z, rhs.z);
}

bool
Point3::operator!=(const Point3 &rhs) const
{
	return !this->operator==(rhs);
}

Mat4
create_xy_rotation_matrix(float sin, float cos)
{
	Mat4 ret{{cos, sin, 0, 0}, {-sin, cos, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
	return ret;
}

Mat4
create_translation_matrix(const Point3 &new_origin)
{
	Mat4 ret = Mat4::get_identity();
	ret[Mat4::Basis::W] = {new_origin.x, new_origin.y, new_origin.z, 1};
	return ret;
}

Mat4
create_uniform_scaling_matrix(float scale)
{
	return create_scaling_matrix(scale, scale, scale);
}

Mat4
create_scaling_matrix(float x, float y, float z)
{
	Mat4 ret = {{x, 0, 0, 0}, {0, y, 0, 0}, {0, 0, z, 0}, {0, 0, 0, 1}};
	return ret;
}

Mat4
create_ortho_matrix(float left, float right, float bottom, float top, float near, float far)
{
	using Basis = Mat4::Basis;
	using Axis = Vec4::Axis;

	float tx = -((right + left) / (right - left));
	float ty = -((top + bottom) / (top - bottom));
	float tz = -((far + near) / (far - near));

	Mat4 ret{Mat4::get_identity()};

	ret[Basis::I][Axis::X] = (2.0) / (right - left);
	ret[Basis::J][Axis::Y] = (2.0) / (top - bottom);
	ret[Basis::K][Axis::Z] = (-2.0) / (far - near);
	ret[Basis::W] = {tx, ty, tz, 1};

	return ret;
}

} // namespace affine
