/*
 * idle-event-queue.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <queue>

namespace orbycomp
{

namespace util
{

template <typename Sig> class EventQueue;
class EventToken;

namespace impl
{

class EventCallbackBase
{
	friend EventToken;

protected:
	bool cancelled{false};

	EventToken *token{nullptr};

public:
	virtual ~EventCallbackBase();
};

template <typename Sig> class EventCallback : public EventCallbackBase
{
	friend EventQueue<Sig>;
	using Func = std::function<Sig>;

	Func cb_;

public:
	EventCallback(Func &&cb) : EventCallbackBase{}, cb_{std::move(cb)} {}
	~EventCallback() {}
};

}; // namespace impl

class EventToken
{
	friend impl::EventCallbackBase;
	impl::EventCallbackBase *cb_base;

public:
	EventToken() : cb_base{nullptr} {}
	EventToken(impl::EventCallbackBase &base) : cb_base{&base} {}

	EventToken(const EventToken &other) = delete;
	EventToken &operator=(const EventToken &other) = delete;

	EventToken(EventToken &&other) : cb_base{std::move(other.cb_base)}
	{
		other.cb_base = nullptr;

		if (cb_base) {
			cb_base->token = this;
		}
	}

	EventToken &
	operator=(EventToken &&other)
	{
		cb_base = std::move(other.cb_base);
		other.cb_base = nullptr;

		if (cb_base) {
			cb_base->token = this;
		}

		return *this;
	}

	~EventToken()
	{
		if (cb_base) {
			cb_base->cancelled = true;
			cb_base->token = nullptr;
			cb_base = nullptr;
		}
	}

	bool
	active() const
	{
		return cb_base != nullptr;
	}
};

template <typename Sig> class EventQueue
{
	friend impl::EventCallback<Sig>;
	friend EventToken;

	using Func = std::function<Sig>;

	std::queue<impl::EventCallback<Sig>> queue_{};

	static_assert(std::is_same<void, typename Func::result_type>::value,
		      "Events callbacks can only be void");

public:
	EventQueue() {}
	~EventQueue() {}

	EventToken
	add_callback(Func &&cb)
	{
		auto &callback = queue_.emplace(std::move(cb));
		auto token = EventToken{callback};

		callback.token = &token;

		return token;
	}

	void
	add_uncancellable_callback(Func &&cb)
	{
		queue_.emplace(std::move(cb));
	}

	bool
	has_callbacks() const
	{
		return !queue_.empty();
	}

	template <typename... Args>
	void
	run_callback(Args... args)
	{
		if (queue_.empty()) {
			return;
		}

		auto &cb = queue_.front();
		if (cb.cancelled) {
			queue_.pop();
			return;
		}

		cb.cb_(args...);
		queue_.pop();
	}
};

}; // namespace util

}; // namespace orbycomp
