/*
 * region.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <algorithm>
#include <iostream>
#include <orbycomp/region.hh>

static inline bool
rect_is_valid(const Region::Rect &rect)
{
	return rect.x1 < rect.x2 && rect.y1 < rect.y2;
}

static bool compare_rects(const Region::Rect &a, const Region::Rect &b);

Region merge_rects(std::vector<Region::Rect> &&rects_);

Region
Region::operator+(const Region &other) const
{
	// Special cases:
	//
	// 1) If both regions are empty, return an empty region
	if (this->rects.size() == 0 && other.rects.size() == 0) {
		return Region();
	}

	// 2) If `this` is empty, return copy of `other`.
	if (this->rects.size() == 0) {
		return Region(other);
	}

	// 3) If `other` is empty, return copy of `this`.
	if (other.rects.size() == 0) {
		return Region(*this);
	}

	// Otherwise, we have to go through the relatively
	// expensive merging process.

	std::vector<Region::Rect> rects(this->rects);
	rects.insert(std::end(rects), std::begin(other.rects), std::end(other.rects));
	std::sort(std::begin(rects), std::end(rects), compare_rects);

	return merge_rects(std::move(rects));
}

Region subtract_rects(std::vector<Region::Rect> &&minuend_, const std::vector<Region::Rect> &subtrahend);

Region
Region::operator-(const Region &other) const
{
	// Special cases:
	//
	// 1) If `this` is empty, return an empty region.
	if (this->rects.size() == 0) {
		return Region();
	}

	// 2) If `other` is empty, return a copy of `this`.
	if (other.rects.size() == 0) {
		return Region(*this);
	}

	return subtract_rects(std::vector<Rect>(this->rects), other.rects);
}

Region
Region::operator+(const Rect &rect) const
{
	// Special case:
	//
	// If `this` is empty, just return a region with the `rect`.
	if (this->rects.size() == 0) {
		return Region(rect);
	}

	std::vector<Region::Rect> rects(this->rects);
	rects.push_back(rect);
	std::sort(std::begin(rects), std::end(rects), compare_rects);

	return merge_rects(std::move(rects));
}

Region
Region::operator-(const Rect &rect) const
{
	// Special case:
	//
	// If `this` is empty, just return a region with the `rect`.
	if (this->rects.size() == 0) {
		return Region(rect);
	}

	std::vector<Region::Rect> minuend(this->rects);
	std::vector<Region::Rect> subtrahend{rect};

	return subtract_rects(std::move(minuend), subtrahend);
}

Region &
Region::operator+=(const Region &other)
{
	*this = this->operator+(other);
	return *this;
}

Region &
Region::operator-=(const Region &other)
{
	*this = this->operator-(other);
	return *this;
}

Region &
Region::operator+=(const Rect &rect)
{
	*this = this->operator+(rect);
	return *this;
}

Region &
Region::operator-=(const Rect &rect)
{
	*this = this->operator-(rect);
	return *this;
}

static bool
compare_rects(const Region::Rect &a, const Region::Rect &b)
{
	bool is_a_valid = rect_is_valid(a);
	bool is_b_valid = rect_is_valid(b);

	if (!is_a_valid) {
		return false;
	}

	if (!is_b_valid) {
		return true;
	}

	// Smaller y-values sort earlier.
	if (a.y1 < b.y1) return true;
	if (a.y1 > b.y1) return false;

	if (a.y2 < b.y2) return true;
	if (a.y2 > b.y2) return false;

	// And if they're equivalent, then the earlier x-values sort earlier
	if (a.x1 < b.x1) return true;
	if (a.x1 > b.x1) return false;

	if (a.x2 < b.x2) return true;
	if (a.x2 > b.x2) return false;

	return false;
}

static inline bool
rects_intersect(const Region::Rect &a, const Region::Rect &b)
{
	return b.x1 < a.x2 && a.x1 < b.x2 && b.y1 < a.y2 && a.y1 < b.y2;
}

static inline Region::Rect
calculate_bounds(const std::vector<Region::Rect> &rects)
{
	if (rects.size() == 0) {
		return Region::Rect{0, 0, 0, 0};
	}

	Region::Rect bounds{
	    std::numeric_limits<std::int32_t>::max(), std::numeric_limits<std::int32_t>::max(),
	    std::numeric_limits<std::int32_t>::min(), std::numeric_limits<std::int32_t>::min()};

	for (const auto &rect : rects) {
		bounds.x1 = std::min(bounds.x1, rect.x1);
		bounds.x2 = std::max(bounds.x2, rect.x2);
		bounds.y1 = std::min(bounds.y1, rect.y1);
		bounds.y2 = std::max(bounds.y2, rect.y2);
	}

	return bounds;
}

Region
merge_rects(std::vector<Region::Rect> &&rects_)
{
	using Rect = Region::Rect;

	std::vector<Rect> rects(std::move(rects_));
	std::vector<Rect> new_rects;

	Rect *pivot = nullptr;
	for (auto &rect : rects) {
		// If this rectangle isn't valid for whatever
		// reason, skip it.
		if (!rect_is_valid(rect)) {
			continue;
		}

		// If we don't have a pivot, set it and continue.
		if (pivot == nullptr) {
			pivot = &rect;
			continue;
		}

		// Because the rectangles are sorted, if the pivot
		// does not intersect with this rectangle, we can
		// safely set the pivot to the current one.
		if (!rects_intersect(*pivot, rect)) {
			new_rects.push_back(*pivot);
			pivot = &rect;
			continue;
		}

		Rect top{pivot->x1, pivot->y1, pivot->x2, rect.y1};
		Rect middle{std::min(pivot->x1, rect.x1), std::min(pivot->y2, rect.y1),
			    std::max(pivot->x2, rect.x2), std::min(pivot->y2, rect.y2)};
		Rect bottom{pivot->x1, rect.y2, pivot->x2, pivot->y2};

		new_rects.push_back(top);
		new_rects.push_back(middle);
		new_rects.push_back(bottom);

		rect.y1 = middle.y2;

		pivot = &rect;
	}

	if (pivot && std::find(std::begin(new_rects), std::end(new_rects), *pivot) == std::end(new_rects)) {
		new_rects.push_back(*pivot);
	}

	new_rects.erase(std::remove_if(std::begin(new_rects), std::end(new_rects),
				       [](const auto &a) { return !rect_is_valid(a); }),
			std::end(new_rects));
	std::sort(std::begin(new_rects), std::end(new_rects), compare_rects);

	Rect bounds = calculate_bounds(new_rects);

	return Region(bounds, std::move(new_rects));
}

// Returns `true` if `a` fully contains `b`
static bool
rect_contains(const Region::Rect &a, const Region::Rect &b)
{
	if (a.x1 > b.x1) {
		return false;
	}
	if (a.x2 < b.x2) {
		return false;
	}
	if (a.y1 > b.y1) {
		return false;
	}
	if (a.y2 < b.y2) {
		return false;
	}
	return true;
}

Region
subtract_rects(std::vector<Region::Rect> &&minuend_, const std::vector<Region::Rect> &subtrahend)
{
	using Rect = Region::Rect;

	std::vector<Rect> minuend(std::move(minuend_));

	bool found_intersection = false;
	do {
		found_intersection = false;

		for (std::vector<Rect>::size_type i = 0; i < minuend.size(); ++i) {
			Rect pivot = minuend[i];

			for (const auto &subtrahend_rect : subtrahend) {
				if (!rect_is_valid(pivot)) {
					break;
				}

				if (!rects_intersect(pivot, subtrahend_rect)) {
					continue;
				}

				found_intersection = true;

				// If the subtrahend fully contains the pivot
				// invalidate the pivot and move on.
				if (rect_contains(subtrahend_rect, pivot)) {
					minuend[i].x2 = pivot.x1;
					minuend[i].y2 = pivot.y1;
					break;
				}

				bool pivot_space_used = false;

				// The rectangles intersect. We now need to
				// split the current "pivot" into four
				// retangles.

				// The "top" rectangle
				if (pivot.y1 < subtrahend_rect.y1) {
					Rect top{pivot.x1, pivot.y1, pivot.x2, subtrahend_rect.y1};
					if (pivot_space_used) {
						minuend.push_back(top);
					} else {
						minuend[i] = top;
						pivot_space_used = true;
					}
				}

				// The "bottom" rectangle
				if (pivot.y2 > subtrahend_rect.y2) {
					Rect bottom{pivot.x1, subtrahend_rect.y2, pivot.x2, pivot.y2};
					if (pivot_space_used) {
						minuend.push_back(bottom);
					} else {
						minuend[i] = bottom;
						pivot_space_used = true;
					}
				}

				// The "left" rectangle
				if (pivot.x1 < subtrahend_rect.x1) {
					Rect left{pivot.x1, std::max(subtrahend_rect.y1, pivot.y1),
						  subtrahend_rect.x1, std::min(subtrahend_rect.y2, pivot.y2)};
					if (pivot_space_used) {
						minuend.push_back(left);
					} else {
						minuend[i] = left;
						pivot_space_used = true;
					}
				}

				// The "right" rectangle
				if (pivot.x2 > subtrahend_rect.x2) {
					Rect right{subtrahend_rect.x2, std::max(subtrahend_rect.y1, pivot.y1),
						   pivot.x2, std::min(subtrahend_rect.y2, pivot.y2)};
					if (pivot_space_used) {
						minuend.push_back(right);
					} else {
						minuend[i] = right;
						pivot_space_used = true;
					}
				}
			}
		}
	} while (found_intersection);

	// Remove all of the invalid rects
	minuend.erase(std::remove_if(std::begin(minuend), std::end(minuend),
				     [](const auto &a) { return !rect_is_valid(a); }),
		      std::end(minuend));

	// And sort the rectangles
	std::sort(std::begin(minuend), std::end(minuend), compare_rects);

	Rect bounds = calculate_bounds(minuend);

	return Region(bounds, std::move(minuend));
}

bool
Region::intersects_with(const Region &other) const
{
	for (const auto &a : *this) {
		for (const auto &b : other) {
			if (rects_intersect(a, b)) {
				return true;
			}
		}
	}

	return false;
}

Region
Region::intersect(const Region &other) const
{
	if (rects.size() == 0) {
		return Region{};
	}

	if (other.rects.size() == 0) {
		return Region{};
	}

	std::vector<Rect> new_rects{};
	new_rects.reserve(rects.capacity());

	for (const auto &a : this->rects) {
		for (const auto &b : other.rects) {
			if (!rects_intersect(a, b)) {
				continue;
			}

			new_rects.push_back({std::max(a.x1, b.x1), std::max(a.y1, b.y1), std::min(a.x2, b.x2),
					     std::min(a.y2, b.y2)});
		}
	}

	std::sort(std::begin(new_rects), std::end(new_rects), compare_rects);

	return merge_rects(std::move(new_rects));
}

bool
Region::operator==(const Region &rhs) const
{
	return bounds == rhs.bounds && rects == rhs.rects;
}

Region::Region(std::initializer_list<Rect> rect_list) : rects{rect_list} { bounds = calculate_bounds(rects); }

Region &
Region::translate(int32_t delta_x, int32_t delta_y)
{
	for (auto &rect : rects) {
		rect.x1 += delta_x;
		rect.y1 += delta_y;
		rect.x2 += delta_x;
		rect.y2 += delta_y;
	}

	return *this;
}

Region
Region::translated(int32_t delta_x, int32_t delta_y) const
{
	std::vector<Rect> new_rects{};
	for (const auto &rect : rects) {
		new_rects.push_back(
		    Rect{rect.x1 + delta_x, rect.y1 + delta_y, rect.x2 + delta_x, rect.y2 + delta_y});
	}

	Rect bounds = calculate_bounds(new_rects);
	return Region(bounds, std::move(new_rects));
}

bool
Region::contains_point(int32_t x, int32_t y) const
{
	for (const auto &rect : rects) {
		if (rect.x1 <= x && rect.x2 > x && rect.y1 <= y && rect.y2 > y) {
			return true;
		}
	}

	return false;
}

std::ostream &
operator<<(std::ostream &stream, const Region::Rect &rect)
{
	stream << "{";
	stream << "(" << rect.x1 << ", " << rect.y1 << ")";
	stream << "-";
	stream << "(" << rect.x2 << ", " << rect.y2 << ")";
	stream << "}";
	return stream;
}

std::ostream &
operator<<(std::ostream &stream, const Region &region)
{
	stream << "{";
	bool first = true;
	for (const auto &rect : region) {
		stream << (first ? "" : ",") << rect;
	}
	stream << "}";
	return stream;
}
