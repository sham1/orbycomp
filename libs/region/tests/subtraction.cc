/*
 * subtraction.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <orbycomp/region.hh>

using Rect = Region::Rect;

SCENARIO("Regions and rectangles can be subtracted from regions", "[subtract]")
{
	GIVEN("A region with a rectangle from (0, 0) to (10, 10)")
	{
		Region region(Rect{0, 0, 10, 10});

		WHEN("a rectangle from (3, 3) to (7, 7) is subtracted")
		{
			Rect rect{3, 3, 7, 7};
			region -= rect;

			THEN("there will be four rectangles") { REQUIRE(region.size() == 4); }

			AND_THEN("the first rectangle is from (0, 0) to (10, 3)")
			{
				REQUIRE(region[0] == Rect{0, 0, 10, 3});
			}

			AND_THEN("the second rectangle is from (0, 3) to (3, 7)")
			{
				REQUIRE(region[1] == Rect{0, 3, 3, 7});
			}

			AND_THEN("the second rectangle is from (7, 3) to (10, 7)")
			{
				REQUIRE(region[2] == Rect{7, 3, 10, 7});
			}

			AND_THEN("the second rectangle is from (0, 7) to (10, 10)")
			{
				REQUIRE(region[3] == Rect{0, 7, 10, 10});
			}
		}
	}

	AND_GIVEN("A region with a rectangle from (1, 1) to (3, 3)")
	{
		Region region({1, 1, 3, 3});

		WHEN("a region is subtracted, where the first rectangle is "
		     "from (0, 0) to (4, 2) & "
		     "the second rectangle is from (0, 2) to (2, 5)")
		{
			Region other_region;
			other_region += Rect{0, 0, 4, 2};
			other_region += Rect{0, 2, 2, 5};

			region -= other_region;

			THEN("there will still only be one rectangle") { REQUIRE(region.size() == 1); }

			AND_THEN("the rectangle will be from (2, 2) to (3, 3)")
			{
				REQUIRE(region[0] == Rect{2, 2, 3, 3});
			}
		}
	}
}
