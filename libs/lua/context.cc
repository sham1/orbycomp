/*
 * context.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <cstring>
#include <orbycomp/lua.hh>
#include <orbycomp/util/format-string.hh>
#include <vector>

namespace orbycomp
{

namespace lua
{

Context::Context() : L_{luaL_newstate()}
{
	if (!L_) {
		throw std::bad_alloc();
	}
}

Context::~Context() {}

Context
Context::create_with_stdlibs()
{
	Context c;

	luaL_openlibs(c.L());

	return c;
}

void
Context::wrap_with_try(struct lua_State *L, std::function<void(LuaWrapper &)> &&f_, bool &had_error)
{
	had_error = false;
	Context ctx{L};
	std::function<void(LuaWrapper &)> f{std::move(f_)};
	try {
		LuaWrapper wrapper{&ctx};
		f(wrapper);
	} catch (std::exception &ex) {
		lua_pushstring(L, ex.what());
		had_error = true;
	}

	ctx.L_.release();
}

void
Context::wrap_with_exceptions(struct lua_State *L, std::function<void(LuaWrapper &)> &&f)
{
	bool had_error{false};
	Context::wrap_with_try(L, std::move(f), had_error);

	if (had_error) {
		lua_error(L);
	}
}

void
Context::wrap_function_real(struct lua_State *L,
			    std::function<int(LuaWrapper &)> &&f,
			    int &ret_count,
			    bool &had_error)
{

	Context::wrap_with_try(
	    L, [&ret_count, &f](LuaWrapper &L) { ret_count = f(L); }, had_error);
}

int
Context::wrap_function(struct lua_State *L, std::function<int(LuaWrapper &)> &&f)
{
	int ret_count{0};
	bool had_error{false};

	wrap_function_real(L, std::move(f), ret_count, had_error);

	if (had_error) {
		lua_error(L);
	}

	return ret_count;
}

namespace impl
{

[[noreturn]] void throw_tag_error(struct lua_State *L, int arg, int tag);

void
push(struct lua_State *L, std::nullptr_t)
{
	lua_checkstack(L, 1);
	lua_pushnil(L);
}

void
push(struct lua_State *L, const std::string &str)
{
	lua_checkstack(L, 1);
	lua_pushlstring(L, str.c_str(), str.length());
}

void
push(struct lua_State *L, lua_Integer n)
{
	lua_checkstack(L, 1);
	lua_pushinteger(L, n);
}

void
push(struct lua_State *L, lua_Unsigned n)
{
	lua_checkstack(L, 1);
	lua_pushinteger(L, n);
}

void
push(struct lua_State *L, lua_Number n)
{
	lua_checkstack(L, 1);
	lua_pushnumber(L, n);
}

void
push(struct lua_State *L, bool b)
{
	lua_checkstack(L, 1);
	lua_pushboolean(L, b);
}

void
push(struct lua_State *L, int n)
{
	lua_checkstack(L, 1);
	lua_pushinteger(L, n);
}

void
push(struct lua_State *L, unsigned int n)
{
	lua_checkstack(L, 1);
	lua_pushinteger(L, n);
}

void
push(struct lua_State *L, const char *str)
{
	lua_checkstack(L, 1);
	lua_pushstring(L, str);
}

void
push(struct lua_State *L, lua_CFunction func)
{
	lua_checkstack(L, 1);
	lua_pushcfunction(L, func);
}

template <>
std::nullptr_t
get<std::nullptr_t>(struct lua_State *L, int stack_pos)
{
	if (lua_isnil(L, stack_pos)) {
		return nullptr;
	} else {
		throw_tag_error(L, stack_pos, LUA_TNIL);
	}
}

template <>
std::string
get<std::string>(struct lua_State *L, int stack_pos)
{
	if (lua_isstring(L, stack_pos)) {
		size_t len{0};
		const char *s = lua_tolstring(L, stack_pos, &len);
		return std::string{s, len};
	} else {
		throw_tag_error(L, stack_pos, LUA_TSTRING);
	}
}

template <>
lua_Integer
get<lua_Integer>(struct lua_State *L, int stack_pos)
{
	int is_num{0};
	lua_Integer n = lua_tointegerx(L, stack_pos, &is_num);
	if (is_num) {
		return n;
	} else {
		throw_tag_error(L, stack_pos, LUA_TNUMBER);
	}
}

template <>
lua_Unsigned
get<lua_Unsigned>(struct lua_State *L, int stack_pos)
{
	int is_num{0};
	lua_Integer n = lua_tointegerx(L, stack_pos, &is_num);
	if (is_num) {
		return n;
	} else {
		throw_tag_error(L, stack_pos, LUA_TNUMBER);
	}
}

template <>
lua_Number
get<lua_Number>(struct lua_State *L, int stack_pos)
{
	int is_num{0};
	lua_Number n = lua_tonumberx(L, stack_pos, &is_num);
	if (is_num) {
		return n;
	} else {
		throw_tag_error(L, stack_pos, LUA_TNUMBER);
	}
}

template <>
bool
get<bool>(struct lua_State *L, int stack_pos)
{
	return lua_toboolean(L, stack_pos);
}

template <>
std::nullptr_t
get_unchecked<std::nullptr_t>(struct lua_State *L, int stack_pos)
{
	return nullptr;
}

template <>
std::string
get_unchecked<std::string>(struct lua_State *L, int stack_pos)
{
	size_t len{0};
	const char *s = lua_tolstring(L, stack_pos, &len);
	return std::string{s, len};
}

template <>
lua_Integer
get_unchecked<lua_Integer>(struct lua_State *L, int stack_pos)
{
	return lua_tointeger(L, stack_pos);
}

template <>
lua_Unsigned
get_unchecked<lua_Unsigned>(struct lua_State *L, int stack_pos)
{
	return lua_tointeger(L, stack_pos);
}

template <>
lua_Number
get_unchecked<lua_Number>(struct lua_State *L, int stack_pos)
{
	return lua_tonumber(L, stack_pos);
}

template <>
bool
get_unchecked<bool>(struct lua_State *L, int stack_pos)
{
	return lua_toboolean(L, stack_pos);
}

/**
 * This is a part that mimics and is based upon
 * Lua's lauxlib.c.
 */

[[noreturn]] void
throw_tag_error(struct lua_State *L, int arg, int tag)
{
	throw_type_error(L, arg, lua_typename(L, tag));
}

void
throw_type_error(struct lua_State *L, int arg, const std::string &tname)
{
	std::string typearg;
	if (luaL_getmetafield(L, arg, "__name") == LUA_TSTRING) {
		typearg = get<std::string>(L, -1);
	} else if (lua_type(L, arg) == LUA_TLIGHTUSERDATA) {
		typearg = "light userdata";
	} else {
		typearg = std::string(luaL_typename(L, arg));
	}

	std::string msg(util::format_str("%s expected, got %s", tname.c_str(), typearg.c_str()));

	throw_argument_error(L, arg, msg);
}

static bool
find_field(struct lua_State *L, int objidx, int level)
{
	if (level == 0 || !lua_istable(L, -1)) {
		return false;
	}

	lua_pushnil(L);
	while (lua_next(L, -2)) {
		// Check if key is a string
		if (lua_type(L, -2) == LUA_TSTRING) {
			// Check if we have found the object we're looking for
			if (lua_rawequal(L, objidx, -1)) {
				// We remove the value but keep the name
				lua_pop(L, 1);
				return true;
			} else if (find_field(L, objidx, level - 1)) {
				// We'll try recursion here.
				// Stack is currently:
				// lib_name, lib_table, field_name (top)

				// Let's put a dot between the lib_name and
				// field_name
				lua_pushliteral(L, ".");
				// Replace the lib_table with said dot
				lua_replace(L, -3);
				// And concatenate
				lua_concat(L, 3);
				return true;
			}
		}
		lua_pop(L, 1);
	}

	return false;
}

static bool
find_global_funcname(struct lua_State *L, lua_Debug *ar, std::string &name)
{
	int top = lua_gettop(L);
	lua_getinfo(L, "f", ar);
	lua_getfield(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE);
	if (find_field(L, top + 1, 2)) {
		auto found_name = get<std::string>(L, -1);
		// Now we have our name. Check if the name starts with '_G.'
		// If so, remove it
		if (std::strncmp(found_name.c_str(), LUA_GNAME ".", 3) == 0) {
			name = std::string(found_name.begin() + 3, found_name.end());
		} else {
			name = std::string(found_name);
		}

		// And clean up!
		lua_settop(L, top);
		return true;
	} else {
		// Didn't find, just pop everything pushed
		lua_settop(L, top);
		return false;
	}

	// Never reached, but compiler might still be annoyed.
	return false;
}

void
throw_argument_error(struct lua_State *L, int arg, const std::string &extramsg)
{
	lua_Debug ar;
	if (!lua_getstack(L, 0, &ar)) {
		throw_error(L, util::format_str("bad argument #%d (%s)", arg, extramsg.c_str()));
	}

	lua_getinfo(L, "n", &ar);
	if (std::strcmp(ar.namewhat, "method") == 0) {
		// Let's not count `self' just like in upstream
		arg--;
		if (arg == 0) {
			// But if the error was in self argument...
			throw_error(
			    L, util::format_str("calling '%s' on bad self (%s)", ar.name, extramsg.c_str()));
		}
	}

	std::string global_func_name;
	if (ar.name == NULL) {
		bool found = find_global_funcname(L, &ar, global_func_name);
		if (!found) {
			global_func_name = "?";
		}
	} else {
		global_func_name = std::string(ar.name);
	}

	throw_error(L, util::format_str("bad argument #%d to '%s' (%s)", arg, global_func_name.c_str(),
					extramsg.c_str()));
}

std::string
get_error_loc(struct lua_State *L, int level)
{
	lua_Debug ar;
	if (lua_getstack(L, level, &ar)) {
		lua_getinfo(L, "Sl", &ar);
		if (ar.currentline > 0) {
			return util::format_str("%s:%d: ", ar.short_src, ar.currentline);
		}
	}
	return "";
}

void
throw_error(struct lua_State *L, const std::string &msg)
{
	std::string full_msg = get_error_loc(L, 1) + msg;
	throw std::runtime_error(full_msg);
}

}; // namespace impl

} // namespace lua

} // namespace orbycomp
