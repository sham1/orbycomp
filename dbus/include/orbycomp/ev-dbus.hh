/*
 * ev-dbus.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <dbus/dbus.h>
#include <ev.h>
#include <memory>
#include <string>

namespace orbycomp
{

namespace dbus
{

class Context
{
	// DBusWatch support
	class Watcher
	{
		Context &context_;
		DBusWatch *watch_;

		struct ev_io watcher {
		};

		static void watcher_fini(void *user_data);
		static void on_watch_cb(EV_P_ struct ev_io *w, int revents);

	public:
		Watcher(Context &context, DBusWatch *watch);
		~Watcher();

		static dbus_bool_t add_watch_cb(DBusWatch *watch, void *user_data);
		static void remove_watch_cb(DBusWatch *watch, void *user_data);
		static void toggle_watch_cb(DBusWatch *watch, void *user_data);
	};

	// DBusTimeout support
	class Timeout
	{
		Context &context_;
		DBusTimeout *timeout_;

		struct ev_timer timer {
		};

		static void timeout_fini(void *user_data);
		static void on_timer_cb(EV_P_ struct ev_timer *w, int revents);

		void adjust_timer();

	public:
		Timeout(Context &context, DBusTimeout *timeout);
		~Timeout();

		static dbus_bool_t add_timeout_cb(DBusTimeout *timeout, void *user_data);
		static void remove_timeout_cb(DBusTimeout *timeout, void *user_data);
		static void toggle_timeout_cb(DBusTimeout *timeout, void *user_data);
	};

	using Connection = std::unique_ptr<DBusConnection, decltype(&dbus_connection_unref)>;

	struct ev_loop *loop_;
	Connection connection_;

	struct ev_async dispatch_dbus;

	Context(struct ev_loop *&loop, Connection &&connection);

	static void dispatch_dbus_cb(EV_P_ struct ev_async *w, int revents);
	static void wakeup_main_loop_cb(void *user_data);

public:
	class SignalMatcher
	{
		bool active{false};
		Context *context;
		std::string matcher_str;

		friend Context;

		SignalMatcher(Context &ctx,
			      const std::string &sender,
			      const std::string &iface,
			      const std::string &member,
			      const std::string &path);

	public:
		SignalMatcher() = default;
		~SignalMatcher();

		SignalMatcher &operator=(SignalMatcher &&other);
	};

	~Context();

	static Context get_from_bus(struct ev_loop *loop, DBusBusType bus);

	DBusConnection *get_connection();

	SignalMatcher add_signal_matcher(const std::string &sender,
					 const std::string &iface,
					 const std::string &member,
					 const std::string &path);
};

// A convenient and RAII-friendly wrapper for DBusError
class Error
{
	DBusError inner{};

public:
	Error() { dbus_error_init(&inner); }

	~Error() { dbus_error_free(&inner); }

	inline operator bool() const { return dbus_error_is_set(&inner); }

	inline DBusError *
	operator&()
	{
		return &inner;
	}

	inline std::string
	name() const
	{
		if (!this->operator bool()) {
			return {};
		}
		return inner.name;
	}

	inline std::string
	message() const
	{
		if (!this->operator bool()) {
			return {};
		}
		return inner.message;
	}
};

// A convenient wrapper around an std::unique_ptr containing DBusMessage
class Message
{
	std::unique_ptr<DBusMessage, decltype(&dbus_message_unref)> inner;

public:
	Message(DBusMessage *msg) : inner{msg, dbus_message_unref} {}
	Message(std::unique_ptr<DBusMessage, decltype(&dbus_message_unref)> other) : inner{std::move(other)}
	{
	}
	Message(Message &&other) : inner{std::move(other.inner)} {}

	~Message() = default;

	inline DBusMessage *
	get()
	{
		return inner.get();
	}

	inline operator bool() { return inner.operator bool(); }
};

} // namespace dbus

} // namespace orbycomp
